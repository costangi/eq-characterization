import multiprocessing

import numpy as np
from joblib import Parallel, delayed

from geo_lib.okada import forward as okada85


def _modeled_displacement_stations_sample(i, station_coordinates, U, L):
    """
    Generates a sample of co-seismic displacement by modeling a synthetic dislocation using the Okada's dislocation
    model.

    Parameters
    ----------
    i : index of the sample (used in the parallel loop)
    station_coordinates : coordinate of the GNSS stations
    U : array containing realizations of a uniform random variable
    L : array containing realizations of a lognormal random variable

    Returns
    -------
    displacement : the modeled co-seismic displacement
    epi_lat : latitude of the fault centroid
    epi_lon : longitude of the fault centroid
    hypo_depth : depth of the fault centroid
    Mw : moment magnitude of the generated synthetic earthquake
    strike : strike angle of the synthetic dislocation
    dip : dip angle of the synthetic dislocation
    rake : rake angle of the synthetic dislocation
    u : average slip on the synthetic dislocation
    stress_drop : stress drop associated to the synthetic rupture
    """
    epi_lat = 35.0 + (41.0 - 35.0) * U[i, 0]
    epi_lon = 139.0 + (146.0 - 139.0) * U[i, 1]
    hypo_depth = 2.0 + (100.0 - 2.0) * U[i, 2]  # depth from 2 to 100
    strike = 160 + 80 * U[i, 3]  # strike from 160 to 240 deg
    dip = 20 + 10 * U[i, 4]  # dip from 20 to 30 deg
    rake = 75 + 25 * U[i, 6]  # rake from 75 to 100 deg
    Mw = 5.8 + (8.5 - 5.8) * U[i, 5]
    Mo = 10 ** (1.5 * Mw + 9.1)
    stress_drop = L[i]
    R = (7 / 16 * Mo / stress_drop) ** (1 / 3)
    u = 16 / (7 * np.pi) * stress_drop / 30e09 * R * 10 ** 3  # converted in mm in order to have displacement in mm
    L = np.sqrt(2 * np.pi) * R  # meters
    W = L / 2
    # L and W must be converted in km
    L = L * 10 ** (-3)
    W = W * 10 ** (-3)

    displacement = okada85((station_coordinates[:, 1] - epi_lon) * 111.3194,
                           (station_coordinates[:, 0] - epi_lat) * 111.3194, 0, 0,
                           hypo_depth + W / 2 * np.sin(np.deg2rad(dip)), L, W, u, 0, strike, dip, rake)
    return displacement, epi_lat, epi_lon, hypo_depth, Mw, strike, dip, rake, u, stress_drop


def modeled_displacement(n_samples, station_coordinates, n_dir):
    """
    Generates 'n_samples' synthetic displacements (cf. '_modeled_displacement_stations_sample(...)').

    Parameters
    ----------
    n_samples : number of samples
    station_coordinates : coordinates of the GNSS stations
    n_dir : number of directions (e.g., N-S, E-W, Vertical)

    Returns
    -------
    disp_stations : modeled co-seismic displacement collected at each station
    y : target variables (latitude, longitude, depth, magnitude)
    catalogue : full source parameters (cf. '_modeled_displacement_stations_sample(...)')
    """
    n_stations = station_coordinates.shape[0]
    disp_stations = np.zeros((n_samples, n_stations, 3))
    Uniform = np.random.uniform(0, 1, (n_samples, 7))
    catalogue = np.zeros((n_samples, 9))
    y = np.zeros((n_samples, 4))
    # lognormal number generator
    mean_stress_drop = 12.62
    std_stress_drop = 2.14
    Lognorm = np.random.lognormal(mean_stress_drop, std_stress_drop, (n_samples,))

    results = Parallel(n_jobs=multiprocessing.cpu_count(), verbose=True)(
        delayed(_modeled_displacement_stations_sample)(i, station_coordinates, Uniform, Lognorm) for i in
        range(n_samples))

    for ns in range(n_samples):
        for direc in range(n_dir):
            disp_stations[ns, :, direc] = results[ns][0][direc]
        y[ns, :] = results[ns][1:5]
        catalogue[ns, :] = results[ns][1:]

    return disp_stations, y, catalogue
