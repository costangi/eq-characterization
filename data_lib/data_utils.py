import os

import joblib
import numpy as np
from sklearn.preprocessing import MinMaxScaler

from utils import honshu_coordinates, honshu_filtered_stations


def _get_synthetic_data(data_filename):
    """
    Loads synthetic data.

    Parameters
    ----------
    data_filename : path of the file to open, which contains the synthetic data

    Returns
    -------
    X : the requested synthetic data (train, val or test)
    y : target variables
    """
    with np.load(data_filename) as f:
        X, y = f['X'], f['y']

    X = X[..., :2]  # horizontal only
    depth = y[:, 2]
    y = y[:, (0, 1, 3)]  # lat, lon, mw
    return X, y


def _get_real_data(data_filename, data_type):
    """
    Loads real data.

    Parameters
    ----------
    data_filename : path of the file to open, which contains the real data
    data_type : string indicating the type of data (e.g., time series, images, image time series)

    Returns
    -------
    X : the requested real data
    """
    with np.load(data_filename) as f:
        X = f[data_type]

    X = X[..., (1, 0)]  # horizontal only (E-W before)
    return X


def scaler_exists(scaler_filename):
    """
    Check wheter the scaler located at 'scaler_filename' exists.

    Parameters
    ----------
    scaler_filename : name of the file where the scaler is supposed to be

    Returns
    -------
    True if the scaler exists at the requested location, False otherwise
    """
    return os.path.isfile(scaler_filename)


def _get_scaler(scaler_filename='../mm_scaler'):
    """
    Loads a scaler from the specified path, if it exists, otherwise a new scaler is instantiated.

    Parameters
    ----------
    scaler_filename : name of the file where the scaler is supposed to be

    Returns
    -------
    mm : an instance of sklearn.preprocessing.MinMaxScaler
    scaler_filename : forwards the input parameter
    """
    if scaler_exists(scaler_filename):
        mm = joblib.load(scaler_filename)
    else:
        mm = MinMaxScaler()
    return mm, scaler_filename


def _save_scaler(scaler, scaler_filename):
    """
    Saves a scalar into a file.

    Parameters
    ----------
    scaler : an instance of sklearn.preprocessing.MinMaxScaler
    scaler_filename : path where to save the scaler
    """
    if not scaler_exists(scaler_filename):
        joblib.dump(scaler, scaler_filename)


def prepare_synthetic_data(data_filename, model_name):
    """
    Prepares synthetic data for train, validation or test phase.

    Parameters
    ----------
    data_filename : path where data is stored
    model_name : name of the deep learning model (e.g., TS, IMG, TRA)

    Returns
    -------
    train_data : the training data
    ytr : the scaled train output variables
    val_data : the validation data
    yv : the scaled validation output variables
    test_data : the test data
    y_test : the scaled test output variables
    mm : the sklearn.preprocessing.MinMaxScaler instance used to scale the output variables
    """
    X, y = _get_synthetic_data(data_filename)

    n_samples = X.shape[0]

    train_frac = 0.6
    val_frac = 0.2

    ind_val = int(n_samples * train_frac)
    ind_test = int(n_samples * (train_frac + val_frac))

    X_train, X_val, X_test = X[:ind_val], X[ind_val:ind_test], X[ind_test:]
    y_train, y_val, y_test = y[:ind_val], y[ind_val:ind_test], y[ind_test:]
    # depth_train, depth_val, depth_test = depth[:ind_val], depth[ind_val:ind_test], depth[ind_test:]

    mm, scaler_filename = _get_scaler()
    ytr = mm.fit_transform(y_train)
    yv = mm.transform(y_val)
    # yt = mm.transform(y_test)
    _save_scaler(mm, scaler_filename)

    train_data = X_train
    val_data = X_val
    test_data = X_test

    if model_name == 'TS':
        _, n_stations, WIN_LENGTH, _ = X.shape
        station_codes, station_coordinates = honshu_coordinates() if n_stations == 300 else honshu_filtered_stations()

        mm_scaler_stations = MinMaxScaler()
        scaled_coords = mm_scaler_stations.fit_transform(station_coordinates)

        station_coords_train = np.transpose(np.tile(scaled_coords, (ind_val, 1, 1, 1)), (0, 2, 1, 3))
        station_coords_val = np.transpose(np.tile(scaled_coords, (ind_test - ind_val, 1, 1, 1)), (0, 2, 1, 3))
        station_coords_test = np.transpose(np.tile(scaled_coords, (n_samples - ind_test, 1, 1, 1)), (0, 2, 1, 3))

        train_data = [X_train, station_coords_train, np.ones((ind_val, n_stations))]
        val_data = [X_val, station_coords_val, np.ones((ind_test - ind_val, n_stations))]
        test_data = [X_test, station_coords_test, np.ones((n_samples - ind_test, n_stations))]

    return train_data, ytr, val_data, yv, test_data, y_test, mm


def prepare_real_data(data_filename, model_name, data_type):
    """
    Prepares synthetic data for the test phase.

    Parameters
    ----------
    data_filename : path where data is stored
    model_name : name of the deep learning model (e.g., TS, IMG, TRA)
    data_type : string indicating the type of data (e.g., time series, images, image time series)

    Returns
    -------
    test_data : the requested real data
    data_scaler : the instance of the sklearn.preprocessing.MinMaxScaler instance used to scale
    the synthetic output variables
    """
    X = _get_real_data(data_filename, data_type)
    data_scaler, _ = _get_scaler()
    n_samples = X.shape[0]
    test_data = X

    if model_name == 'TS':
        _, n_stations, WIN_LENGTH, _ = X.shape
        station_codes, station_coordinates = honshu_coordinates() if n_stations == 300 else honshu_filtered_stations()

        mm_scaler_stations = MinMaxScaler()
        scaled_coords = mm_scaler_stations.fit_transform(station_coordinates)
        station_coords_test = np.transpose(np.tile(scaled_coords, (n_samples, 1, 1, 1)), (0, 2, 1, 3))
        test_data = [X, station_coords_test, np.ones((n_samples, n_stations))]
    return test_data, data_scaler
