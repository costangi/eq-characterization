import multiprocessing

import numpy as np
import pandas as pd
import pygmt
from joblib import Parallel, delayed

from data_lib.model_data import modeled_displacement
from utils import honshu_coordinates, honshu_filtered_stations

def _image_sample(obs_points, data, mask, region, n_dir, pix_lat, pix_lon, incr_sur='5m', incr_smooth='25m'):
    """
    Generates images by interpolating in space by means of adjustable tension continuous curvature splines.

    Parameters
    ----------
    obs_points : GNSS stations
    data : displacement values recorded at each GNSS station
    mask : inland mask
    region : box indicating the region of interest
    n_dir : number of directions (e.g., N-S, E-W, Vertical)
    pix_lat : number of pixel for latitude quantization (height of the final image)
    pix_lon : number of pixel for longitude quantization (width of the final image)
    incr_sur : spacing for surface generation
    incr_smooth : spacing for the anti-aliasing filter

    Returns
    -------
    images : interpolated images for each direction
    """
    images = np.zeros((pix_lat, pix_lon, n_dir))
    for i in range(n_dir):
        smoothed = pygmt.blockmedian(data=pd.DataFrame(
            np.hstack((obs_points, data[:, i].reshape(-1, 1)))),
            region=region, spacing=incr_smooth, verbose='q')
        smoothed = smoothed.to_numpy()

        surface = pygmt.surface(x=smoothed[:, 0], y=smoothed[:, 1],
                                z=smoothed[:, 2], region=region,
                                spacing=incr_sur, T=0.25, verbose='q')
        surface = surface.to_pandas().to_numpy()
        masked = np.multiply(surface, mask)
        images[:, :, i] = masked
    return images


def _image_time_series_sample(obs_points, data, mask, region, n_dir, pix_lat, pix_lon, length, incr_sur='5m',
                              incr_smooth='25m'):
    """
    Generates image time series by interpolating each frame in space by means of adjustable tension
    continuous curvature splines.

    Parameters
    ----------
    obs_points : GNSS stations
    data : displacement values recorded at each GNSS station
    mask : inland mask
    region : box indicating the region of interest
    n_dir : number of directions (e.g., N-S, E-W, Vertical)
    pix_lat : number of pixel for latitude quantization (height of the final image)
    pix_lon : number of pixel for longitude quantization (width of the final image)
    length : number of frames in the final image (default: 100)
    incr_sur : spacing for surface generation
    incr_smooth : spacing for the anti-aliasing filter

    Returns
    -------
    img_time_series : interpolated image time series for each direction
    """
    img_time_series = np.zeros((pix_lat, pix_lon, length, n_dir))
    for x in range(length):
        img_time_series[:, :, x, :] = _image_sample(obs_points, data[:, x, :], mask, region, n_dir, pix_lat, pix_lon,
                                          incr_sur=incr_sur,
                                          incr_smooth=incr_smooth)
    return img_time_series


def _modeled_time_series(n_samples, station_coordinates, n_dir, win_length=100):
    """
    Generates time series of synthetic displacement from Okada's dislocation model, assumed as a Heaviside step
    centered at the center of the window, equal to the co-seismic displacement for each GNSS station.

    Parameters
    ----------
    n_samples : number of input samples
    station_coordinates : coordinates of the GNSS stations
    n_dir : number of directions (e.g., N-S, E-W, Vertical)
    win_length : length of the considered window (default: 100)

    Returns
    -------
    mod_time_series : synthetic time series from Okada's model
    y: target variables (latitude, longitude, depth, magnitude) [depth is kept for compatibility]
    catalogue: full source parameters (see 'modeled_displacement(...)')
    """
    disp_stations, y, catalogue = modeled_displacement(n_samples, station_coordinates, n_dir)
    _, n_stations, _ = disp_stations.shape
    mod_time_series = np.zeros((n_samples, n_stations, win_length, 3))
    mod_time_series[:, :, win_length // 2:, :] = disp_stations[:, :, np.newaxis, :]
    return mod_time_series, y, catalogue


def synthetic_time_series(n_samples_ts, noise):
    """
    Generates synthetic time series, assumed as the sum of a noise time series and a modeled
    signal (cf. '_modeled_time_series(...)').

    Parameters
    ----------
    n_samples_ts : number of samples
    noise : noise time series

    Returns
    -------
    synth_time_series : synthetic time series (noise + modeled signal)
    y : target variables (latitude, longitude, depth, magnitude) (forwarded from '_modeled_time_series(...)')
    catalog : full source parameters (forwarded from '_modeled_time_series(...)')
    """
    _, n_stations, win_size, n_dir = noise.shape
    station_codes, station_coordinates = honshu_coordinates() if n_stations == 300 else honshu_filtered_stations()
    mod_time_series, y, catalog = _modeled_time_series(n_samples_ts, station_coordinates, n_dir, win_length=win_size)
    synth_time_series = noise[:n_samples_ts] + mod_time_series
    return synth_time_series, y, catalog


def differential_images(time_series):
    """
    Generates spatially-interpolated differential images obtained from the extracted co-seismic displacement
    (difference of the displacement at the day after and the day before). The co-seismic date is assumed to be
    at the center of the time series.

    Parameters
    ----------
    time_series : time series from which to compute the differential images.

    Returns
    -------
    images : the differential images
    """
    n_samples, n_stations, win_size, n_dir = time_series.shape
    station_codes, station_coordinates = honshu_coordinates() if n_stations == 300 else honshu_filtered_stations()
    region = [station_coordinates[:, 1].min(), station_coordinates[:, 1].max(), station_coordinates[:, 0].min(),
              station_coordinates[:, 0].max()]
    mask = pygmt.datasets.load_earth_relief(region=region, resolution='05m')
    mask = mask.to_pandas().to_numpy()
    mask = mask > 0
    pixel_lat, pixel_lon = mask.shape
    images = np.zeros((n_samples, pixel_lat, pixel_lon, n_dir))
    results = Parallel(n_jobs=multiprocessing.cpu_count(), verbose=True)(
        delayed(_image_sample)(station_coordinates[:, (1, 0)],
                               time_series[i, :, win_size // 2 + 1, :] - time_series[i, :, win_size // 2 - 1, :], mask,
                               region, n_dir, pixel_lat, pixel_lon) for i in range(n_samples))

    for i in range(n_samples):
        images[i] = results[i]
    return images


def image_time_series(time_series, length=15):
    """
    Generates spatially-interpolated differential image time series.

    Parameters
    ----------
    time_series : time series from which to compute the differential images.
    length : length of the image time series (default: 15)
    Returns
    -------
    images : the differential images
    """
    n_samples, n_stations, win_size, n_dir = time_series.shape
    station_codes, station_coordinates = honshu_coordinates() if n_stations == 300 else honshu_filtered_stations()

    region = [station_coordinates[:, 1].min(), station_coordinates[:, 1].max(), station_coordinates[:, 0].min(),
              station_coordinates[:, 0].max()]
    mask = pygmt.datasets.load_earth_relief(region=region, resolution='05m')
    mask = mask.to_pandas().to_numpy()
    mask = mask > 0
    pixel_lat, pixel_lon = mask.shape
    image_time_series = np.zeros((n_samples, pixel_lat, pixel_lon, length, n_dir))
    results = Parallel(
        n_jobs=multiprocessing.cpu_count(), verbose=True)(delayed(_image_time_series_sample)(
        station_coordinates[:, (1, 0)],
        time_series[i, :, win_size // 2 - length // 2:win_size // 2 + length // 2 + 1, :],
        mask, region, n_dir, pixel_lat, pixel_lon, length) for i in range(n_samples))

    for i in range(n_samples):
        image_time_series[i] = results[i]

    image_time_series = np.transpose(image_time_series, axes=(0, 3, 1, 2, 4))  # permuted for TRA requirements

    return image_time_series
