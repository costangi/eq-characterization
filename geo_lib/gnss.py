import datetime
import multiprocessing
from os import path

import numpy as np
import pandas as pd
from joblib import Parallel, delayed


def _time_align(data, time, ref_time):
    """
    Aligns time series with the reference time ('ref_time').

    Parameters
    ----------
    data : time series.
    time : time assciated to the 'data' input
    ref_time : time array containing the reference time that the data has to be aligned with

    Returns
    -------
    new_data : the aligned time series
    """
    new_data = np.zeros((len(ref_time), data.shape[1]))
    data_ind = 0
    for ind, t in enumerate(ref_time):
        if t in time:
            new_data[ind] = data[data_ind]
            data_ind += 1
        else:
            new_data[ind] = np.nan
    return new_data


def _remove_outliers(input_series, window_size, n_sigmas=3):
    """
    Removes outliers by means of the hampel filter (from: https://pypi.org/project/hampel/).

    Parameters
    ----------
    input_series : input time series
    window_size : size of the sliding window
    n_sigmas : multiple of the standard deviation used by the algorithm (default: 3 according to the Pearson's rule)

    Returns
    -------
    new_series : time series from which the outliers have been removed
    indices : indices of the found outliers
    """
    n = len(input_series)
    new_series = input_series.copy()
    k = 1.4826  # scale factor for Gaussian distribution
    indices = []
    # possibly use np.nanmedian
    for i in range((window_size), (n - window_size)):
        x0 = np.median(input_series[(i - window_size):(i + window_size)])
        S0 = k * np.median(np.abs(input_series[(i - window_size):(i + window_size)] - x0))
        if np.abs(input_series[i] - x0) > n_sigmas * S0:
            new_series[i] = x0
            indices.append(i)
    return new_series, indices


def _get_series(filename, time_array):
    """
    Loads a GNSS time series specified at 'filename' by aligning the recording with the reference time array
    in input.

    Parameters
    ----------
    filename : path of the GNSS time series to be loaded
    time_array : reference time array

    Returns
    -------
    new_data : the loaded time series (possibly an array filled with 'np.nan' if the path does not exist)
    """
    if path.exists(filename):
        data, time_seq, time_mask = read_from_pos(filename, time_array)
        new_data = _time_align(data, time_seq, time_array)  # fill data gaps with nan
        for i in range(3):
            new_data[:, i] = _remove_outliers(new_data[:, i], 3)[0]
    else:
        new_data = np.full((len(time_array), 3), fill_value=np.nan)
        print(filename, 'does not exist')
    return new_data


def load_gnss_data(station_names, start_time, end_time, method='gamit'):
    """
    Loads GNSS data for the given station names in the specified time interval.

    Parameters
    ----------
    station_names : names of the stations from which to laod the GNSS data
    start_time : starting date for the data loading
    end_time : ending date for the data loading
    method : GNSS solution (either 'gamit' or 'ngl')

    Returns
    -------
    data : the loaded data
    time_array : global time array, serves as reference
    """
    time_array = pd.date_range(start=start_time, end=end_time).to_pydatetime().tolist()
    if method == 'gamit':
        base_folder = '/Users/costangi/Desktop/gps data japan/gamit/tssum'
        suffix = '.uga.tmp_itr14'
    if method == 'ngl':
        base_folder = '/Users/costangi/Desktop/gps data japan/ngl/POS_FILES'
        suffix = ''
    n_stations = len(station_names)
    data = np.zeros((n_stations, len(time_array), 3))

    results = Parallel(n_jobs=multiprocessing.cpu_count(), verbose=True)(
        delayed(_get_series)(path.join(base_folder, name + suffix + '.pos'), time_array) for name in station_names)
    for k, name in enumerate(station_names):
        data[k] = results[k]
    return data, time_array


def read_from_pos(filename, time_array_list):
    """
    Reads GNSS data from a .pos file.

    Parameters
    ----------
    filename
    time_array_list

    Returns
    -------
    disp_data : the loaded GNSS displacement data
    time_seq : time array contained in the GNSS data
    time_mask : mask relative to the time array
    """
    disp_data, time_seq, time_mask = [], [], []
    with open(filename) as f:
        [next(f) for _ in range(37)]
        for line in f:
            lin = line.rstrip()
            spl = lin.split(' ')
            filtered_data = np.array(list(filter(None, spl)))  # we remove empty strings
            data = filtered_data[[15, 16, 17]].astype(np.float) * 1000  # mm [dN dE dU]
            time = datetime.datetime.strptime(filtered_data[0], '%Y%m%d')
            if time < time_array_list[0] or time > time_array_list[-1]:
                continue
            disp_data.append(data)
            time_seq.append(time)
            time_mask.append(time_array_list.index(time))
    disp_data, time_seq = np.array(disp_data), np.array(time_seq)
    return disp_data, time_seq, time_mask
