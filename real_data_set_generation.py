import datetime

import numpy as np
from scipy.signal import detrend

from data_lib.data_representations import differential_images, image_time_series
from geo_lib.gnss import load_gnss_data
from utils import honshu_coordinates, fit_step


def create_time_series(save=True):
    """
    Loads real GNSS data for both the ISTerre/DD and NGL/PPP solutions.

    Parameters
    ----------
    save : boolean indicating if the data needs to be saved

    Returns
    -------
    data_isterre : GNSS data for the ISTerre/DD solution
    time_array_isterre : time array associated to the ISTerre/DD solution
    data_ngl : GNSS data for the NGL/PPP solution
    time_array_ngl : time array associated to the NGL/PPP solution
    """
    codes, _ = honshu_coordinates()
    data_isterre, time_array_isterre = load_gnss_data(codes, datetime.datetime(1995, 1, 1),
                                                      datetime.datetime(2019, 12, 31), method='gamit')
    data_ngl, time_array_ngl = load_gnss_data(codes, datetime.datetime(2009, 1, 1), datetime.datetime(2021, 7, 9),
                                              method='ngl')
    if save:
        np.savez('time_series_isterre', data=data_isterre, t=time_array_isterre)
        np.savez('time_series_ngl', data=data_ngl, t=time_array_ngl)
    return data_isterre, time_array_isterre, data_ngl, time_array_ngl


def load_eq_catalog():
    """
    Loads the NIED earthquake catalog, stored under: './geo_data/catalog_hoshu_1998_2021_5_8_9_5_max_mw_clean.txt'.

    Returns
    -------
    catalog : the earthquake catalog as target output variable (latitude, longitude, depth, magnitude)
    fmec : the focal mechanism for each event
    nodal : the nodal plane for each event
    """
    catalog = dict()  # will contain the date as key, [lat,lon,depth,mw] as value
    fmec = dict()  # will contain the date as key, fmec as value
    nodal = dict()  # will contain the date as key, nodal plane as value

    with open('geo_data/catalog_hoshu_1998_2021_5_8_9_5_max_mw_clean.txt') as f:
        next(f)  # skip header
        for line in f:
            lin = line.rstrip()
            spl = lin.split(',')
            year, month, day = spl[0].split('/')
            cat_line = np.array([spl[2], spl[3], spl[11], spl[12]], dtype=np.float)
            date = datetime.datetime(int(year), int(month), int(day))
            catalog[date] = cat_line
            fmec[date] = np.array([spl[i].split(';')[0] for i in range(7, 10)], dtype=np.float)
            nodal[date] = np.array([spl[i].split(';')[1] for i in range(7, 10)], dtype=np.float)

    print('# events in the NIED catalog:', len(catalog.keys()))

    start_date = min(list(catalog.keys()))
    end_date = max(list(catalog.keys()))
    return catalog, fmec, nodal


def create_data_set(catalog, focal_mechanism, nodal_plane, time_array, time_series, solution_name, save=True):
    """
    Creates the real data set for a given solution, by extracting data windows associated with catalogued earthquakes.

    Parameters
    ----------
    catalog : NIED catalog
    focal_mechanism : focal mechanisms from NIED catalog
    nodal_plane : nodal plane from NIED catalog
    time_array : time array for the given solution
    time_series : GNSS data for the given solution
    solution_name : GNSS processing solution (e.g., ISTerre/DD, NGL/PPP)
    save

    Returns
    -------
    X : windows extracted for each earthquake date in the NIED catalog
    y : target variables for the given date (latitude, longitude, depth, magnitude)
    dates : earthquake dates
    """
    WIN_SIZE = 100
    PERCENTAGE_COVERAGE = 0.6
    PERCENTAGE_DAYS = 0.7
    n_stations = time_series.shape[0]

    X, y, dates, fm, nodal = [], [], [], [], []

    for cat_date in catalog.keys():
        # extract a stack of time series corresponding to the date (50 days before and 50 after)
        # search_index -> index in time series corresponding to the date as datetime
        if not cat_date in time_array.tolist():
            continue
        search_index = time_array.tolist().index(cat_date)
        if search_index - WIN_SIZE // 2 < 0 or search_index + WIN_SIZE // 2 > len(time_array):
            continue
        # we extract a stack when:
        # 1. the PERCENTAGE_COVERAGE of the stations is ON
        # 2. the median of the #registered days is at least PERCENTAGE_DAYS of the window
        # --> the #2 does not incorporates the #1 --> that's why we use also the #2 (the # of NaNs would skew the distribution)
        potential_candidate = time_series[:, search_index - 50:search_index + 50]
        # we take the N-S direction for the computation (equivalent to E-W)

        days_on_per_station = np.sum(~np.isnan(potential_candidate[:, :, 0]), axis=1)
        median_days_on = np.median(days_on_per_station)
        number_stations_off = np.count_nonzero(days_on_per_station == 0)

        if number_stations_off < int(n_stations * (1 - PERCENTAGE_COVERAGE)) and median_days_on > int(
                WIN_SIZE * PERCENTAGE_DAYS):
            # the potential candidate time series are detrended and interpolated
            potential_time_series = np.full(potential_candidate.shape, np.nan)
            for station in range(n_stations):
                for direction in range(2):
                    # print('COUNT', np.count_nonzero(~np.isnan(potential_candidate[station,:,direction])))
                    if np.count_nonzero(~np.isnan(potential_candidate[station, :, direction])) > int(
                            PERCENTAGE_DAYS * WIN_SIZE):
                        fitted = fit_step(np.linspace(0, WIN_SIZE, WIN_SIZE), WIN_SIZE // 2,
                                          potential_candidate[station, :, direction])
                        # print(fitted)
                        potential_time_series[station, :, direction] = detrend(fitted)

            X.append(potential_time_series)
            y.append(catalog[cat_date])
            dates.append(cat_date)
            fm.append(focal_mechanism[cat_date])
            nodal.append(nodal_plane[cat_date])

    X = np.array(X)
    y = np.array(y)

    print('Data length:', X.shape[0])
    if save:
        np.savez(f'data_set_catalog_{solution_name}', X=X, y=y, dates=dates, fm=fm, nodal=nodal)
    return X, y, dates


def create_full_data_set(time_series, solution_name, save=True):
    """
    Creates a data set for the given solution, containing time series, differential images and image time series.

    Parameters
    ----------
    time_series : real GNSS time series
    solution_name : GNSS processing solution (e.g., ISTerre/DD, NGL/PPP)
    save : wheter to save the data set

    Returns
    -------
    time_series : real GNSS time series for the given solution
    diff_images : real GNSS differential images for the given solution
    img_time_series : real GNSS image time series for the given solution
    """
    diff_images = differential_images(time_series)
    img_time_series = image_time_series(time_series, length=image_time_series_length)
    if save:
        np.savez(f'{solution_name}_data', time_series=time_series, differential_images=diff_images,
                 image_time_series=img_time_series)
    return time_series, diff_images, img_time_series


if __name__ == '__main__':
    save_data_set = False
    image_time_series_length = 15
    data_isterre, time_array_isterre, data_ngl, time_array_ngl = create_time_series(save=save_data_set)
    catalog, focal_mechanism, nodal_plane = load_eq_catalog()
    X_isterre, y_isterre, dates_isterre = create_data_set(catalog, focal_mechanism, nodal_plane,
                                                          time_array_isterre, data_isterre, 'isterre_dd',
                                                          save=save_data_set)
    X_ngl, y_ngl, dates_ngl = create_data_set(catalog, focal_mechanism, nodal_plane, time_array_ngl,
                                              data_ngl, 'ngl_ppp', save=save_data_set)
    time_series_isterre, diff_images_isterre, img_time_series_isterre = create_full_data_set(X_isterre, 'isterre_dd',
                                                                                             save=save_data_set)
    time_series_ngl, diff_images_ngl, img_time_series_ngl = create_full_data_set(X_ngl, 'ngl_ppp', save=save_data_set)
