import sys

import numpy as np
import tensorflow as tf

from data_lib.data_utils import prepare_real_data
from ml_lib.IMG_model import IMG
from ml_lib.TRA_model import TRA
from ml_lib.TS_model import TS

if __name__ == '__main__':
    admissible_model_names = ['TS', 'IMG', 'TRA']
    models = [TS, IMG, TRA]
    data_types = ['time_series', 'differential_images', 'image_time_series']

    if len(sys.argv) <= 1:
        raise Exception('Please provide CLI arguments.')
    model_name, data_filename, weight_filename, solution = sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]
    if model_name not in admissible_model_names:
        raise Exception('Please provide a valid model name.')

    model_idx = admissible_model_names.index(model_name)
    model = models[model_idx]()
    data_type = data_types[model_idx]
    test_data, data_scaler = prepare_real_data(data_filename, model_name, data_type)

    if model_name == 'TS':
        _, n_stations, WIN_LENGTH, _ = test_data[0].shape
        params = {
            "N_t": WIN_LENGTH,
            "N_sub": n_stations,
            "dropout_rate": 0.15,
            "activation": "relu",
            "n_outputs": data_scaler.n_features_in_
        }

        model.set_params(params)

    model = model.build()
    model.summary()

    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
        loss='mean_squared_error'
    )

    model.load_weights(weight_filename)

    tmp_res = model.predict(test_data)
    y_pred = data_scaler.inverse_transform(tmp_res)
    np.savez(f'pred_real_{model_name}_{solution}', pred=y_pred)
