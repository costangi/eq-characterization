# eq-characterization



Official source code for the paper "Seismic source characterization from GNSS data using deep learning" (link to the paper).

This software package contains all the source codes for:

- synthetic data generation (`synth_data_set_generation.py`)

- the deep learning models (see the `ml_lib` folder)

- train and evaluation of the models on synthetic and real data.

The noise time series can be downloaded at: [10.5281/zenodo.12706271](https://doi.org/10.5281/zenodo.12706271).

