import tensorflow as tf


class IMG:

    def __init__(self):
        self.dropout_rate = 0.5
        self.n_pixel_lat = 76
        self.n_pixel_lon = 36
        self.input_shape = (self.n_pixel_lat, self.n_pixel_lon, 2)
        self.n_outputs = 3

    def set_params(self, params):
        """
        Update model parameters
        """
        self.__dict__.update(params)

    def build(self):
        base_model = tf.keras.applications.MobileNetV2(include_top=False, weights=None, input_shape=self.input_shape)
        base_model.trainable = True
        inputs = tf.keras.Input(self.input_shape)
        x = base_model(inputs)
        x = tf.keras.layers.GlobalAveragePooling2D()(x)
        x = tf.keras.layers.Dropout(self.dropout_rate)(x, training=True)
        outputs = tf.keras.layers.Dense(self.n_outputs, activation='linear')(x)
        model = tf.keras.Model(inputs, outputs)
        model.build(input_shape=self.input_shape)
        self.model = model
        return model
