import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Conv2D, MaxPool2D, Reshape, RepeatVector
from tensorflow.keras.layers import Dense, Input, Flatten, Dropout, concatenate
from tensorflow.keras.layers import BatchNormalization, Lambda, Multiply, Permute
from tensorflow.keras.layers import Activation, SpatialDropout2D, UpSampling2D
from tensorflow.keras.layers import GaussianNoise, GaussianDropout
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import plot_model

import numpy as np
import pandas as pd
import os
import pickle


class TS:
    """Based on the model from M. van den Ende"""

    def __init__(self):
        self.kernel = (1, 5)
        self.f0 = 2
        self.use_bn = False
        self.use_dropout = True
        self.dropout_at_test = True
        self.dropout_rate = 0.1
        self.LR = 1e-3
        self.initializer = keras.initializers.GlorotUniform()
        self.activation = "relu"
        self.N_sub = 300
        self.N_t = 100
        self.data_shape_waveforms = (self.N_sub, self.N_t, 2)
        self.data_shape_coords = (self.N_sub, 1, 2)
        self.data_shape_weights = (self.N_sub,)
        self.n_outputs = 3

    def set_params(self, params):
        """
        Update model parameters
        """
        self.__dict__.update(params)
        self.data_shape_waveforms = (self.N_sub, self.N_t, 2)
        self.data_shape_coords = (self.N_sub, 1, 2)
        self.data_shape_weights = (self.N_sub,)

    def conv_layer(self, x, filters, kernel_size, use_bn=False, use_dropout=False, activ=None, dropout_at_test=False):
        """
        Convolution layer > batch normalisation > activation > dropout
        """
        use_bias = True
        if use_bn:
            use_bias = False

        x = Conv2D(
            filters=filters, kernel_size=kernel_size, padding="same",
            activation=None, kernel_initializer=self.initializer,
            use_bias=use_bias
        )(x)

        if use_bn:
            x = BatchNormalization()(x)

        if activ is not None:
            x = Activation(activ)(x)

        if use_dropout:
            x = SpatialDropout2D(self.dropout_rate)(x, training=dropout_at_test)

        return x

    def build(self):
        """
        Construct Graph Neural Network
        """

        f = self.f0
        kernel = self.kernel
        use_bn = self.use_bn
        use_dropout = self.use_dropout
        dropout_at_test = self.dropout_at_test
        activation = self.activation

        data_shape = self.data_shape_waveforms
        data_shape2 = self.data_shape_coords
        data_shape3 = self.data_shape_weights

        input_data = Input(data_shape)
        input_coords = Input(data_shape2)
        input_weights = Input(data_shape3)

        x = input_data

        """
        Component 1: CNN that processes station waveforms
        """

        # (t, f): (Nb, Ns, 2048, 3) -> (Nb, Ns, 512, 8) -> (Nb, Ns, 128, 16) -> (Nb, Ns, 32, 32)
        # Construct 3 blocks, each with 3 convolutional layers
        for i in range(3):
            # Double number of filters each block
            f = f * 2
            for j in range(3):
                x = self.conv_layer(
                    x, filters=f, kernel_size=kernel, use_bn=use_bn, use_dropout=use_dropout,
                    activ=activation, dropout_at_test=dropout_at_test
                )
            # Downsample time axis
            x = MaxPool2D(pool_size=(1, 4))(x)

            # Last block is special (no final dropout or downsampling)
        # (Nb, Ns, 32, 32) -> (Nb, Ns, 32, 64)
        f = f * 2
        # Create 2 layers
        for i in range(2):
            x = self.conv_layer(
                x, filters=f, kernel_size=kernel, use_bn=use_bn, use_dropout=use_dropout,
                activ=activation, dropout_at_test=dropout_at_test
            )
        # Final layer. Do not dropout!
        x = self.conv_layer(
            x, filters=f, kernel_size=kernel, use_bn=False, use_dropout=False,
            activ="relu", dropout_at_test=dropout_at_test
        )

        # Reduce: (Nb, Ns, 32, 64) -> (Nb, Ns, 1, 64)
        x = Lambda(lambda x: tf.reduce_max(x, axis=2, keepdims=True))(x)
        x = GaussianDropout(self.dropout_rate)(x)
        # Concatenate: (Nb, Ns, 1, 64) -> (Nb, Ns, 1, 64+2)
        x = concatenate([x, input_coords])

        """
        Component 2: MLP that processes extracted features and location
        """

        # Location processing (Nb, Ns, 1, 64+2) -> (Nb, Ns, 1, 64)
        x = self.conv_layer(
            x, filters=128, kernel_size=(1, 1), use_bn=False, use_dropout=use_dropout,
            activ=activation, dropout_at_test=dropout_at_test
        )
        # Final layer. Do not dropout!
        x = self.conv_layer(
            x, filters=128, kernel_size=(1, 1), use_bn=False, use_dropout=False,
            activ=activation, dropout_at_test=dropout_at_test
        )

        # Reshape weights: (Nb, Ns) -> (Nb, Ns, 64) -> (Nb, Ns, 1, 64)
        aggr_weights_reshape = RepeatVector(x.shape[-1])(input_weights)
        aggr_weights_reshape = Permute([2, 1])(aggr_weights_reshape)
        aggr_weights_reshape = tf.keras.backend.expand_dims(aggr_weights_reshape, axis=-2)

        # Reduce: (Nb, Ns, 1, 64) -> (Nb, 1, 1, 64) -> (Nb, 64)
        x = x * aggr_weights_reshape
        x = Lambda(lambda x: tf.reduce_max(x, axis=1, keepdims=True))(x)
        x = Flatten()(x)
        x = GaussianDropout(self.dropout_rate)(x)

        """
        Component 3: MLP that combines aggregated node attributes 
        to predict source characteristics
        """

        # Source characterisation (Nb, 64) -> (Nb, 4)
        x = Dense(128, activation=activation, kernel_initializer=self.initializer)(x)
        x = GaussianDropout(self.dropout_rate)(x, training=dropout_at_test)

        x = Dense(self.n_outputs, activation="linear", kernel_initializer=self.initializer)(x)
        x = Lambda(lambda x: x * 1, name="prediction")(x)

        # Build and compile model
        model = Model([input_data, input_coords, input_weights], x)
        model.build(input_shape=(data_shape, data_shape2, data_shape3))

        self.model = model
        return model
