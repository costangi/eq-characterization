import tensorflow as tf

from ml_lib.positional_embedding import PositionalEmbedding
from ml_lib.transformer_utils import _transformer


class TRA:

    def __init__(self):
        self.transformer_dropout_rate = 0.1
        self.final_dropout_rate = 0.5
        self.n_pixel_lat = 76
        self.n_pixel_lon = 36
        self.sequence_length = 15
        self.embedding_dimension = 1280
        self.input_shape = (self.sequence_length, self.n_pixel_lat, self.n_pixel_lon, 2)
        self.feature_extractor_shape = (self.n_pixel_lat, self.n_pixel_lon, 2)
        self.n_outputs = 3

    def set_params(self, params):
        """
        Update model parameters
        """
        self.__dict__.update(params)

    def build(self):
        inputs = tf.keras.Input(shape=self.input_shape)
        extr = tf.keras.applications.MobileNetV2(include_top=False, weights=None,
                                                 input_shape=self.feature_extractor_shape)
        extr.trainable = True

        x = tf.keras.layers.TimeDistributed(extr, name='feature_extraction')(inputs)
        pool = tf.keras.layers.GlobalAveragePooling2D()
        x = tf.keras.layers.TimeDistributed(pool, name='avg_pooling')(x)
        # transformer
        x = PositionalEmbedding(
            self.sequence_length, self.embedding_dimension, name="frame_position_embedding"
        )(x)
        x, x1 = _transformer(self.transformer_dropout_rate, None, 'transformer_layer', x)

        x = tf.keras.layers.GlobalMaxPooling1D()(x)
        x = tf.keras.layers.Dropout(self.final_dropout_rate)(x, training=True)
        outputs = tf.keras.layers.Dense(self.n_outputs, activation="linear", name='prediction')(x)
        model = tf.keras.Model(inputs, outputs)
        model = tf.keras.Model(inputs, outputs)
        model.build(input_shape=self.input_shape)
        self.model = model
        return model
