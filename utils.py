import numpy as np
from scipy.stats import linregress
from sklearn.metrics import euclidean_distances


def honshu_coordinates():
    """
    Loads GNSS station coordinates for the Honshu region, Japan, without considering the antenna height.

    Returns
    -------
    station_codes : codes of the loaded GNSS stations
    station_coordinates : coordinates of the loaded GNSS stations
    """
    with open('geo_data/Honshu_coords.txt') as f:
        rows = f.read().splitlines()
        station_codes = []
        station_coordinates = []
        for row in rows[1:]:  # skip the header
            station_code, station_lat, station_lon, _ = row.split(' ')
            station_codes.append(station_code)
            station_coordinates.append([station_lat, station_lon])
        station_coordinates = np.array(station_coordinates, dtype=np.float)
    return station_codes, station_coordinates


def honshu_coordinates_full():
    """
    Loads GNSS station coordinates for the Honshu region, Japan, by considering the antenna height as well.

    Returns
    -------
    station_codes : codes of the loaded GNSS stations
    station_coordinates : coordinates of the loaded GNSS stations
    """
    with open('geo_data/Honshu_coords.txt') as f:
        rows = f.read().splitlines()
        station_codes = []
        station_coordinates = []
        for row in rows[1:]:  # skip the header
            station_code, station_lat, station_lon, station_height = row.split(' ')
            station_codes.append(station_code)
            station_coordinates.append([station_lat, station_lon, station_height])
        station_coordinates = np.array(station_coordinates, dtype=np.float)
        # convert height in km and then in degrees
        station_coordinates[:, 2] = station_coordinates[:, 2] * 0.001 * (360 / 40075)
    return station_codes, station_coordinates


def honshu_filtered_stations():
    """
    Filters GNSS stations in Honshu out from 'noisy' stations.

    Returns
    -------
    station_codes : codes of the filtered GNSS stations
    station_coordinates : coordinates of the filtered GNSS stations
    mask : mask indicating the non-noisy stations
    """
    station_codes, station_coordinates = honshu_coordinates()
    not_operational = [3, 36, 13, 15, 94, 137, 143, 169, 171, 221, 42, 237, 262, 267, 278, 294]
    not_operational_stations = []
    for ind in not_operational:
        not_operational_stations.append(station_codes[ind])

    for ind in not_operational_stations:
        station_codes.remove(ind)

    mask = np.ones(station_coordinates.shape[0], dtype=np.bool)

    # indices = [237, 267, 278, 294, 40, 207, 48, 281]
    mask[not_operational] = False
    station_coordinates = station_coordinates[mask]
    return station_codes, station_coordinates, mask


def get_depth_ranges():
    """
    Returns depth ranges used for plots in Costantino et al., 2022.
    
    Returns
    -------
    depth_ranges : the depth ranges
    """
    depth_ranges = [(0, 30), (30, 60), (60, 100)]
    return depth_ranges


def get_distance_ranges():
    """
    Returns distance ranges (in arc degrees) used for plots in Costantino et al., 2022.

    Returns
    -------
    distance_ranges : the distance ranges
    """
    distance_ranges = [(0, 0.5), (0.5, 3), (3, 100000)]
    return distance_ranges


def get_mw_thresh_ranges():
    """
    Returns the magnitude threshold matrix as described in Costantino et al., 2022.

    Returns
    -------
    mw_tresh_matrix : array([[6. , 6.8, 7.5],
                             [6.2, 6.8, 7.5],
                             [6.5, 7. , 7.8]])
    """
    mw_tresh_matrix = np.array([[6, 6.8, 7.5], [6.2, 6.8, 7.5], [6.5, 7., 7.8]])
    return mw_tresh_matrix


def get_distance_near_station_event(catalog_line):
    """
    Returns the 3D distance to the nearest GNSS station for a single earthquake.

    Parameters
    ----------
    catalog_line

    Returns
    -------
    min_distance : the distance to the nearest GNSS station
    """
    min_distance = np.min(euclidean_distances(honshu_coordinates_full()[1], np.array(
        [catalog_line[0], catalog_line[1], -catalog_line[2] * (360 / 40075)]).reshape(-1, 1).T))
    return min_distance


def get_distance_nearest_station_full(catalog):
    """
    Computes the distance to the nearest GNSS station for all the earthquakes in a given catalog

    Parameters
    ----------
    catalog : the input catalog

    Returns
    -------
    nearest_station_actual : the array of distances to the nearest GNSS station
    """
    nearest_station_actual = np.zeros((len(catalog)))
    for i in range(len(catalog)):
        nearest_station_actual[i] = np.min(get_distance_near_station_event(catalog[i]))
    return nearest_station_actual


def characterizable_event(catalog_line):
    """
    Computes if an event is characterizable or not, according to the criterion defined in Costantino et al., 2022.

    Parameters
    ----------
    catalog_line : an event

    Returns
    -------
    True if the event is characterizable, False otherwise
    """
    Mw = catalog_line[3]
    depth = catalog_line[2]

    # getting the depth range
    for k in range(3):
        if depth > get_depth_ranges()[k][0] and depth < get_depth_ranges()[k][1]:
            break
    depth_ind = k

    for k in range(3):
        if get_distance_near_station_event(catalog_line) > get_distance_ranges()[k][
            0] and get_distance_near_station_event(catalog_line) < get_distance_ranges()[k][1]:
            break
    dist_ind = k

    # now we access the matrix to retrieve the Mw thresh
    # print('distance:', get_distance_near_station_event(catalog_line), Mw, depth)
    # print('indices dep-dis:', depth_ind, dist_ind)
    if Mw > get_mw_thresh_ranges()[depth_ind, dist_ind]:
        return True
    return False


def characterizable_catalog(catalog):
    """
    Computes if all the events in a catalog are characterizable (cf. 'characterizable_event(...)').

    Parameters
    ----------
    catalog : the input catalog

    Returns
    -------
    mask : a boolean array for each event indicating its characterizabilitu
    """
    mask = np.array([characterizable_event(event) for event in catalog], dtype=np.bool)
    return mask


def isthrust(fmec, aux):
    """
    Test wheter an event has a thrust focal mechanism.

    Parameters
    ----------
    fmec : focal mechanism
    aux : auxiliary plane

    Returns
    -------
    True if the event is a thrust, False otherwise
    """
    if fmec[0] > 160 and fmec[0] < 240 and fmec[1] > 5 and fmec[1] < 65 and fmec[2] > 45 and fmec[2] < 135:
        return True
    elif aux[0] > 160 and aux[0] < 240 and aux[1] > 5 and aux[1] < 65 and aux[2] > 45 and aux[2] < 135:
        return True
    else:
        return False


def thrust_indices(focal_mechanism_catalog, nodal_planes_catalog):
    """
    Computes the indices of thrust and non-thrust events in the catalog.

    Parameters
    ----------
    focal_mechanism_catalog : catalog of all the focal mechanisms
    nodal_planes_catalog : catalog of all the nodal planes

    Returns
    -------
    ind_thrust : indices of all thrust events
    ind_not_thrust : indices of all non-thrust events
    """
    ind_thrust = np.zeros((focal_mechanism_catalog.shape[0],), dtype=np.bool)
    ind_not_thrust = np.ones((focal_mechanism_catalog.shape[0],), dtype=np.bool)
    for i in range(focal_mechanism_catalog.shape[0]):
        if isthrust(focal_mechanism_catalog[i], nodal_planes_catalog[i]):
            ind_thrust[i] = 1
            ind_not_thrust[i] = 0
    return ind_thrust, ind_not_thrust


def characterizable_color(condition, color_type='rgb'):
    """
    Helper function for event color mapping based on characterizability.

    Parameters
    ----------
    condition : True for characterizable events, False otherwise
    color_type : type of color (e.g., 'rbg', 'string')

    Returns
    -------
    red or blue color, expressed either in rgb or string format
    """
    blue_rgb, red_rgb = [0, 0, 1], [1, 0, 0]
    blue_str, red_str = 'blue', 'red'
    if color_type == 'rgb':
        return blue_rgb if condition else red_rgb
    else:
        return blue_str if condition else red_str


def characterizable_thrust_color(event, fmec, aux, color_type='rgb'):
    """
    Helper function for event color mapping based on characterizability and thrust focal mechanism.

    Parameters
    ----------
    event : the target event
    fmec : the focal mechanism of the target event
    aux : the nodal plane of the target event
    color_type : type of color (e.g., 'rbg', 'string')

    Returns
    -------
    red, green or blue color, expressed either in rgb or string format
    """
    blue_rgb, red_rgb, green_rgb = [0, 0, 1], [1, 0, 0], [0, 1, 0]
    blue_str, red_str, green_str = 'blue', 'red', 'green'
    if characterizable_event(event):
        if isthrust(fmec, aux):
            return green_rgb if color_type == 'rgb' else green_str
        else:
            return blue_rgb if color_type == 'rgb' else blue_str
    else:
        return red_rgb if color_type == 'rgb' else red_str


def characterizable_colors(characterizable_labels):
    """
    Computes the color of a catalog based on the characterizability of the events within.

    Parameters
    ----------
    characterizable_labels : the target catalog

    Returns
    -------
    characterizable_colors : an array of rbg-colors associated with the input catalog
    """
    characterizable_colors = np.zeros((characterizable_labels.shape[0], 3))
    characterizable_colors[characterizable_labels == 0] = characterizable_color(False)
    characterizable_colors[characterizable_labels == 1] = characterizable_color(True)
    return characterizable_colors


def fit_step(x, x0, y):
    """
    Fills the gaps in a time series centered in the time of an earthquake. Two linear functions are estimated to the
    period before and after the shock (step-like function) and the data is interpolated accordingly.

    Parameters
    ----------
    x : the independent variable (time)
    x0 : the value at which the step is found (co-seismic)
    y : the dependent variable (GNSS time series)

    Returns
    -------
    filled : the dependent variable y, after interpolation
    """
    nonan_left = np.argwhere(~np.isnan(y[:x0])).ravel()
    nonan_right = x0 + np.argwhere(~np.isnan(y[x0:])).ravel()

    m_l, b_l, _, _, _ = linregress(x[nonan_left], y[nonan_left])
    m_r, b_r, _, _, _ = linregress(x[nonan_right], y[nonan_right])

    filled_tmp = np.zeros(y.shape)
    filled_tmp[:x0] = m_l * x[:x0] + b_l
    filled_tmp[x0:] = m_r * x[x0:] + b_r

    valid_ind_left = list(set(np.fromiter(range(x0), np.int)).difference(set(nonan_left)))
    valid_ind_right = list(set(np.fromiter(range(x0, x.shape[0]), np.int)).difference(set(nonan_right)))

    filled = np.copy(y)
    filled[valid_ind_left] = filled_tmp[valid_ind_left]
    filled[valid_ind_right] = filled_tmp[valid_ind_right]

    return filled
