import numpy as np
from matplotlib import pyplot as plt

from data_lib.data_representations import synthetic_time_series, differential_images, image_time_series
from noise_lib.artificial_noise import generate_artificial_noise

n_samples = 20000
image_time_series_length = 15

noise = generate_artificial_noise(n_samples)

time_series, target_variables, synthetic_catalog = synthetic_time_series(n_samples, noise)
diff_images = differential_images(time_series)
img_time_series = image_time_series(time_series, length=image_time_series_length)

# you may want to save them on the disk, e.g.:
np.savez('TS_data_set', X=time_series, y=target_variables, catalog=synthetic_catalog)
np.savez('IMG_data_set', X=diff_images, y=target_variables, catalog=synthetic_catalog)
np.savez('TRA_data_set', X=img_time_series, y=target_variables, catalog=synthetic_catalog)
