import datetime
import math
import os
import re
from os import path

import matplotlib
import numpy as np
import pandas as pd
import pygmt
from matplotlib import pyplot as plt
from matplotlib.colors import Normalize
from matplotlib.path import Path
from mpl_toolkits.basemap import Basemap
from prettytable import PrettyTable
from scipy import ndimage, stats
from scipy.interpolate import griddata, PchipInterpolator
from scipy.signal import medfilt
from scipy.stats import median_abs_deviation
from sklearn.decomposition import PCA
from sklearn.metrics import euclidean_distances

from geo_lib.gnss import _get_series
from utils import honshu_coordinates, honshu_coordinates_full, characterizable_catalog, thrust_indices, \
    characterizable_thrust_color, characterizable_colors


def _prepare_folders():
    base = 'figures/figure'
    for i in range(12):
        if i == 2 or i == 3:
            continue
        folder = base + str(i + 1)
        if not os.path.exists(folder):
            os.makedirs(folder)
            if i == 10 or i == 11:
                methods = ['isterre_dd', 'ngl_ppp']
                for method in methods:
                    os.makedirs(folder + '/' + method)


def _get_zone_idx(y_test):
    region1 = [(140.1, 41.3), (141.55, 41.3), (142.17, 39.86), (140.65, 35.0), (138.9, 34.86), (139., 38.1)]
    region2 = [(143.4, 35.), (144.6, 41.4), (146.1, 41.3), (146., 35.)]
    path1 = Path(region1)
    path2 = Path(region2)
    indices_near = path1.contains_points(y_test[:, (1, 0)])
    indices_far = path2.contains_points(y_test[:, (1, 0)])
    indices_interm = []
    for i in range(y_test.shape[0]):
        if not indices_far[i] and not indices_near[i]:
            indices_interm.append(True)
        else:
            indices_interm.append(False)

    indices_interm = np.array(indices_interm, dtype=np.bool_)

    ind_zones = [indices_near, indices_interm, indices_far]
    return ind_zones


def _detrend_nan_1d(x, y):
    # find linear regression line, subtract off data to detrend
    not_nan_ind = ~np.isnan(y)
    detrend_y = np.zeros(y.shape)
    detrend_y.fill(np.nan)
    if y[not_nan_ind].size > 0:
        m, b, r_val, p_val, std_err = stats.linregress(x[not_nan_ind], y[not_nan_ind])
        detrend_y = y - (m * x + b)
    return detrend_y, m, b


def _find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx


def ymd_decimal_year_lookup():
    """Returns a lookup table for (year, month, day) to decimal year, with the convention introduced by Nasa JPL."""
    ymd_decimal_lookup = dict()
    with open('geo_data/decyr.txt', 'r') as f:
        next(f)
        for line in f:
            line = re.sub(' +', ' ', line)
            splitted_line = line.split(' ')
            decimal, year, month, day = splitted_line[1], splitted_line[2], splitted_line[3], splitted_line[4]
            decimal, year, month, day = float(decimal), int(year), int(month), int(day)
            ymd_decimal_lookup[(year, month, day)] = decimal
    return ymd_decimal_lookup


def load_gnss_data_station(station_name, start_time, end_time, method='gamit'):
    time_array = pd.date_range(start=start_time, end=end_time).to_pydatetime().tolist()
    if method == 'gamit':
        base_folder = '/Users/costangi/Desktop/gps data japan/gamit/tssum'
        suffix = '.uga.tmp_itr14'
    if method == 'ngl':
        base_folder = '/Users/costangi/Desktop/gps data japan/ngl/POS_FILES'
        suffix = ''
    # d = np.zeros((len(time_array), 3))
    t = np.zeros(len(time_array))

    d = _get_series(path.join(base_folder, station_name + suffix + '.pos'), time_array)

    return d, time_array


def _station_dependent_noise(time, noise, noise_length=None):
    nonan = np.argwhere(~np.isnan(noise)).ravel()
    comp_nonan = noise[nonan].ravel()
    time_nonan = time[nonan].ravel()
    pchip = PchipInterpolator(time_nonan, comp_nonan)
    time = time[nonan[0]:nonan[-1]]  # excludes NaN periods at the beginning or at the end
    noise = pchip(time)
    noise = noise[np.argwhere(~np.isnan(noise))].ravel()  # pchip sometimes returns NaN values...
    '''if noise_length is not None:
        print('here2')
        ind = np.random.randint(noise_length // 2, noise.size - (noise_length // 2))
        time = time[ind - noise_length // 2: ind + noise_length // 2]
        noise = noise[ind - noise_length // 2: ind + noise_length // 2]'''
    if noise.size % 2 != 0:
        noise = noise[:-1]
    ft = np.fft.fft(noise)
    n = noise.size // 2
    phi_al = 2 * np.pi * np.random.rand(n - 1, )
    phi = np.angle(ft)
    phi[1:n] = phi_al
    phi[phi.size:-n:-1] = -phi_al
    ft_s = np.multiply(np.abs(ft), np.exp(1j * phi))
    synth_nonan = np.fft.ifft(ft_s)
    return time, np.real(synth_nonan)


def _magnitude_scaling(mw):
    # return 0.1 * _nonlinear_scaling(mw)
    return 0.0013 * mw ** 4


def _prepare_data():
    with np.load('./pred/pred_TS_determ.npz') as f:
        pred_TS, stds_TS, depth_test_TS, y_test_TS, err_mw_TS, err_loc_TS = f['pred'], f['stds'], f['depth_test'], f[
            'y_test'], f['err_mw'], f['err_loc']

    with np.load('./pred/pred_IMG_determ.npz') as f:
        pred_IMG, stds_IMG, depth_test_IMG, y_test_IMG, err_mw_IMG, err_loc_IMG = f['pred'], f['stds'], f['depth_test'], \
                                                                                  f['y_test'], f['err_mw'], f['err_loc']

    with np.load('./pred/pred_TRANSF_determ.npz') as f:
        pred_TRA, stds_TRA, depth_test_TRA, y_test_TRA, err_mw_TRA, err_loc_TRA = f['pred'], f['stds'], f['depth_test'], \
                                                                                  f['y_test'], f['err_mw'], f['err_loc']

    station_codes, station_coordinates = honshu_coordinates()
    _, station_coordinates_full = honshu_coordinates_full()

    models = ['TS', 'IMG', 'TRA']
    y_test_list = [y_test_TS, y_test_IMG, y_test_TRA]
    pred_list = [pred_TS, pred_IMG, pred_TRA]
    stds_list = [stds_TS, stds_IMG, stds_TRA]
    depth_test_list = [depth_test_TS, depth_test_IMG, depth_test_TRA]
    return y_test_list, depth_test_list, pred_list, stds_list, models, station_coordinates, station_coordinates_full


def grid(x, y, z, resX=100, resY=100):
    "Convert 3 column data to matplotlib grid"
    xi = np.linspace(min(x), max(x), resX)
    yi = np.linspace(min(y), max(y), resY)
    X, Y = np.meshgrid(xi, yi)
    Z = griddata((x, y), z, (X, Y), 'linear')
    return X, Y, Z


def movingmedian(interval, window_size):
    idx = np.arange(window_size) + np.arange(len(interval) - window_size + 1)[:, None]
    b = [row[row > 0] for row in interval[idx]]
    # return np.array(map(np.median, b))
    return np.array([np.median(c) for c in b])  # This also works


def date_from_float(date_float):
    year = int(math.modf(date_float)[1])
    date_as_datetime = datetime.datetime(year, 1, 1) + datetime.timedelta(days=((date_float - year) * 365))
    return datetime.datetime(date_as_datetime.year, date_as_datetime.month, date_as_datetime.day)


def figure1():
    station_codes, station_coordinates = honshu_coordinates()

    dummy = np.loadtxt('/Users/costangi/Downloads/TS_Honshu/Raw_data/%s.txt' % '0226')
    print(len(dummy[:, 0]))

    date_float_to_ind = dict()
    for i in range(dummy.shape[0]):
        datetime_from_float = date_from_float(dummy[i, 0])
        date_float_to_ind[datetime_from_float] = i

    data = np.zeros((len(station_codes), dummy.shape[0], 2))  # ns - ew

    for i, code in enumerate(station_codes):
        data[i] = np.loadtxt('/Users/costangi/Downloads/TS_Honshu/Raw_data/{0}.txt'.format(code))[:, (1, 2)]

    catalogue = []
    with open('geo_data/catalog_isc_2.txt') as f:
        for line in f:
            lin = line.rstrip()
            spl = lin.split(' ')
            cat_line = []
            for i in range(len(spl)):
                if spl[i] != '':
                    cat_line.append(spl[i])
            catalogue.append(cat_line)

    # catalogue in the format Y M D LAT LON DEPTH MW
    dates_catalogue = dict()
    for i in range(len(catalogue)):
        date = datetime.datetime(int(catalogue[i][0]), int(catalogue[i][1]), int(catalogue[i][2]))
        dates_catalogue[date] = i

    '''for i in range(len(catalogue)):
        if float(catalogue[i][6]) >= 7.:
            print(catalogue[i])

    print(dates_catalogue[datetime.datetime(2011, 7, 10)])'''

    target_date = datetime.datetime(2005, 8, 16)
    # target_date = datetime.datetime(2003, 5, 26)

    target_index = date_float_to_ind[target_date]  # + 45

    # print('equiv date:', datetime.datetime(2003, 5, 26) + datetime.timedelta(days=45));exit(0)
    print("target index -->", target_index)

    # ind_nan = np.argwhere(np.isnan(data[:, target_index, 1]))

    sind = np.argsort(station_coordinates[:, 0])

    '''plt.plot(data[:, target_index, 1][sind])
    plt.plot(data[:, target_index, 0][sind])
    plt.show()'''

    region = [139 - 1, 144 + 1, 35 - 1, 41 + 1]

    fig = pygmt.Figure()
    fig.basemap(region=region, projection="M15c", frame=True)
    fig.coast(land="lightgrey", water="skyblue")
    datax = data[:, target_index + 1, 1] - data[:, target_index - 1, 1]  # [~ind_nan]  # east
    datay = data[:, target_index + 1, 0] - data[:, target_index - 1, 0]  # [~ind_nan]  # north

    # print(datax)
    # print(datay)

    L = np.sqrt(datax ** 2 + datay ** 2)
    # L_max = np.max(L)
    L_max = np.nanmax(L)

    # outlier = np.argwhere(L > 1500)[0][0]
    mask = np.ones((300,), dtype=np.bool_)
    # mask[outlier] = False

    '''plt.plot(datax[mask])
    plt.plot(datay[mask])
    plt.show()'''

    # plt.plot(L[mask]);plt.show()
    print('L max', L_max)
    print('L min', np.min(L))

    mm_unit = 2

    A = np.arctan2(datay, datax) * 180 / np.pi  # rispetto a orizzontale
    x0 = station_coordinates[:, 1]  # [~ind_nan]
    y0 = station_coordinates[:, 0]  # [~ind_nan]
    fig.plot(x=x0[mask], y=y0[mask], style="v0.3c+e", direction=[A[mask], L[mask] / 3], pen="2p", color="black")

    # fig.plot(x=144, y=34.5, style="v0.6c+e", direction=[[0], [mm_unit]], pen="2p", color="black")
    # fig.text(text='{0} mm'.format(ceil(L_max)), x=144.5, y=34.4)

    fig.plot(x=141.4, y=34.5, style="v0.6c+e", direction=[[0], [L_max / 3]], pen="2p", color="black")
    fig.text(text='{0} mm'.format(math.ceil(L_max / 3)), x=141.8, y=34.4)

    # focal_mechanism = dict(strike=int(strike), dip=int(dip), rake=int(rake), magnitude=int(Mw))
    # fig.meca(focal_mechanism, scale="1c", longitude=epi_lon, latitude=epi_lat, depth=depth)

    # triangles for target stations
    '''fig.plot(station_coordinates[target_indices, 1], station_coordinates[target_indices, 0], style='i0.3c', color='red')
    # plot station name text labels
    labels_x = [142, 141.65, 141.05, 140.7]
    labels_y = [40.5, 38.1, 36.55, 35.45]

    fig.text(text=target_stations, x=labels_x, y=labels_y)'''

    # fig.show(method='external')
    fig.savefig('./figures/figure1/real_disp_field.png', transparent=True)

    # Time series
    target_stations = ['0158', '0550', '0042', '3033']
    target_indices = [station_codes.index(i) for i in target_stations]

    real_ew_disp = data[target_indices, target_index - 50:target_index + 50, 1]  # E-W direction

    for i, st in enumerate(target_stations):
        fig, ax = plt.subplots(1, 1, figsize=(14.4, 3.9), dpi=300)
        ax.plot(real_ew_disp[i], linewidth=4)
        ax.set_yticklabels([])
        ax.set_xticklabels([])
        ax.set_xticks([50])
        ax.set_yticks([])
        ax.patch.set_facecolor('white')
        ax.tick_params(axis="x", direction="in")
        ax.tick_params(axis="y", direction="in")
        [u.set_linewidth(5) for u in ax.spines.values()]  # default : 0.8
        # plt.show()
        plt.savefig('./figures/figure1/real_EW_{0}.png'.format(st), bbox_inches='tight', pad_inches=0)
        plt.close(fig)


def figure2():
    # plt.rc('font', family='Helvetica')

    SMALL_SIZE = 8 + 2 + 1 + 2 + 1
    MEDIUM_SIZE = 10 + 2 + 1 + 2
    BIGGER_SIZE = 12 + 2 + 1 + 2

    plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

    stations = ['3032', '3033', '3034']
    data = []
    models = []
    residuals = []
    for station in stations:
        ts = np.loadtxt(
            f'/Users/costangi/Downloads/Honshu-Mw6.4-Mpost6.8-dc1.15-dp1.156-ds1.0-1997.0_2020.0-GX/OUTPUT_FILES/TS_DATA/{station}.txt')[
             :, (0, 1)]  # N-S
        m = np.loadtxt(
            f'/Users/costangi/Downloads/Honshu-Mw6.4-Mpost6.8-dc1.15-dp1.156-ds1.0-1997.0_2020.0-GX/OUTPUT_FILES/QUA/SYNTHETIC_DATA/{station}.txt')[
            :, (0, 1)]  # N-S
        res = np.loadtxt(
            f'/Users/costangi/Downloads/Honshu-Mw6.4-Mpost6.8-dc1.15-dp1.156-ds1.0-1997.0_2020.0-GX/OUTPUT_FILES/QUA/RESIDUAL_DATA/{station}.txt')[
              :, (0, 1)]  # N-S
        data.append(ts[:, 1])
        models.append(m[:, 1])
        residuals.append(res[:, 1])

        time = res[:, 0]

    time_mask = time < 2011.
    time = time[time_mask]
    original_ts = np.array(data)[:, time_mask]
    trajectory_model = np.array(models)[:, time_mask]
    residual_ts = np.array(residuals)[:, time_mask]

    print(original_ts.shape)

    figsize = (10, 3)
    gnss_point_size = 2

    for i, station in enumerate(stations):
        fig = plt.figure(figsize=figsize)
        plt.scatter(time, original_ts[i], s=gnss_point_size, c='C0')
        plt.plot(time, trajectory_model[i], linewidth=1.3, color='C3', label='Trajectory model')
        plt.legend()
        plt.xlabel('Time [years]')
        plt.ylabel('Displacement [mm]')
        plt.tight_layout()
        # plt.show()
        fig.savefig(f'./figures/figure2/{station}_orig_tm.pdf', format='pdf')
        plt.close(fig)

    for i, station in enumerate(stations):
        fig = plt.figure(figsize=figsize)
        plt.scatter(time, residual_ts[i], s=gnss_point_size, c='C0')
        plt.xlabel('Time [years]')
        plt.ylabel('Displacement [mm]')
        plt.tight_layout()
        # plt.show()
        fig.savefig(f'./figures/figure2/{station}_res.pdf', format='pdf')
        plt.close(fig)

        X = residual_ts.T
        X = X - np.nanmean(X, axis=0)

        X = np.nan_to_num(X, nan=0.)

        pca = PCA(n_components=len(stations))
        X_pcaed = pca.fit_transform(X)

        for i, station in enumerate(stations):
            fig = plt.figure(figsize=figsize)
            plt.scatter(time, X_pcaed[:, i], s=gnss_point_size, c='C0')
            plt.xlabel('Time [years]')
            plt.ylabel('Displacement [mm]')
            plt.tight_layout()
            # plt.show()
            fig.savefig(f'./figures/figure2/{station}_pc.pdf', format='pdf')
            plt.close(fig)

        spatial_matrix = np.around(pca.components_, decimals=2)
        fig = plt.figure()
        plt.gca().matshow(spatial_matrix, cmap='Blues')
        for i in range(len(stations)):
            for j in range(len(stations)):
                plt.gca().text(i, j, str(spatial_matrix[j, i]), va='center', ha='center')
        plt.gca().get_xaxis().set_visible(False)
        plt.gca().get_yaxis().set_visible(False)

        fig.savefig(f'./figures/figure2/spatial_matrix.pdf', format='pdf')
        plt.close(fig)

        ft = np.fft.rfft(X_pcaed.T)
        freqs = np.fft.rfftfreq(len(X_pcaed), time[1] - time[0])  # Get frequency axis from the time axis

        for i, station in enumerate(stations):
            fig = plt.figure(figsize=figsize)
            mags = abs(ft[i])  # We don't care about the phase information here
            mags[0] = np.nan
            plt.plot(freqs, mags)
            plt.yscale('log')
            plt.xlabel('cicles / year')
            plt.ylabel('Amplitude spectrum')
            plt.tight_layout()
            fig.savefig(f'./figures/figure2/{station}_ampsp.pdf', format='pdf')
            plt.close(fig)

        # surrogates = pyunicorn.timeseries.Surrogates(X_pcaed)
        # surrogate_pcaed = surrogates.correlated_noise_surrogates(X_pcaed)
        surrogate_pcaed = np.zeros((X_pcaed.shape[0] - 1, len(stations)))
        for i in range(3):
            time2, surrogate_pcaed_i = _station_dependent_noise(time, X_pcaed[:, 0], noise_length=None)
            surrogate_pcaed[:, i] = surrogate_pcaed_i

        for i, station in enumerate(stations):
            fig = plt.figure(figsize=figsize)
            plt.scatter(time2, surrogate_pcaed[:, i], s=gnss_point_size, c='C0')
            plt.xlabel('Time [years]')
            plt.ylabel('Displacement [mm]')
            plt.tight_layout()
            # plt.show()
            fig.savefig(f'./figures/figure2/{station}_pc_surr.pdf', format='pdf')
            plt.close(fig)

        surrogate_ts = pca.inverse_transform(surrogate_pcaed) + pca.mean_

        for i, station in enumerate(stations):
            fig = plt.figure(figsize=figsize)
            plt.scatter(time2, surrogate_ts[:, i], s=gnss_point_size, c='C0')
            plt.xlabel('Time [years]')
            plt.ylabel('Displacement [mm]')
            plt.tight_layout()
            # plt.show()
            fig.savefig(f'./figures/figure2/{station}_surr.pdf', format='pdf')
            plt.close(fig)


def figure5():
    y_test_list, _, pred_list, stds_list, models, _, _ = _prepare_data()
    fig, axes = plt.subplots(3, 3, figsize=(11.4, 7.8), dpi=100, sharey='row', sharex='row')
    for model in range(3):
        y_test = y_test_list[model]
        pred = pred_list[model]
        minlatitude = np.minimum(y_test[:, 0].min(), pred[:, 0].min())
        maxlatitude = np.maximum(y_test[:, 0].max(), pred[:, 0].max())
        minlongitude = np.minimum(y_test[:, 1].min(), pred[:, 1].min())
        maxlongitude = np.maximum(y_test[:, 1].max(), pred[:, 1].max())
        minmag = np.minimum(y_test[:, 3].min(), pred[:, 3].min())
        maxmag = np.maximum(y_test[:, 3].max(), pred[:, 3].max())
        var_lims = ((minlatitude, maxlatitude), (minlongitude, maxlongitude), (minmag, maxmag))
        mean_preds = [pred[:, 0], pred[:, 1], pred[:, 3]]
        actualvals = [y_test[:, 0], y_test[:, 1], y_test[:, 3]]
        MEAN_WIN = 150
        # for i, ax in enumerate(axes.flat):
        for expnum in range(3):
            print(expnum, model)
            axes[expnum][model].plot(var_lims[expnum], var_lims[expnum], "k--")
            axes[expnum][model].scatter(actualvals[expnum], mean_preds[expnum], c=actualvals[2], cmap='turbo',
                                        alpha=0.4, s=4 ** 2)
            sc = axes[expnum][model].scatter(actualvals[expnum], mean_preds[expnum], alpha=1, c=actualvals[2],
                                             cmap='turbo', s=0)  # super dirty
            sorting_order = np.argsort(actualvals[expnum])  # according to actual values
            axes[expnum][model].plot(
                actualvals[expnum][sorting_order][MEAN_WIN // 2:actualvals[expnum].shape[0] - MEAN_WIN // 2 + 1],
                movingmedian(mean_preds[expnum][sorting_order], MEAN_WIN), c='black', linewidth=2,
                label='running median')

            if model == 0:
                axes[expnum][model].set_ylabel("Predicted value")
            if expnum == 2:
                axes[expnum][model].set_xlabel("Actual value")
            axes[expnum][model].set(adjustable='box', aspect='equal')

    vertical_pad = 20  # in points
    horizontal_pad = 10  # in points
    suptitles_col = models
    suptitles_row = ['Latitude [°]', 'Longitude [°]', 'Magnitude']

    for ax, col in zip(axes[0], suptitles_col):
        ax.annotate(col, xy=(0.5, 1), xytext=(0, vertical_pad),
                    xycoords='axes fraction', textcoords='offset points',
                    size='large', ha='center', va='baseline', weight='bold')

    for ax, row in zip(axes[:, 0], suptitles_row):
        ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - horizontal_pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size='large', ha='right', va='center', weight='bold')

    plt.tight_layout()

    cbar = fig.colorbar(sc, ax=axes.ravel().tolist())
    cbar.ax.set_ylabel('Actual magnitude', rotation=270, labelpad=15)

    # plt.suptitle('Actual / predicted plot')
    plt.savefig('figures/figure5/actual_predicted_std_mw_cbar2.pdf', format='pdf', bbox_inches='tight')


def figure6():
    ranges = [(5.8, 6.3), (6.3, 6.8), (6.8, 7.5), (7.5, 8.5)]
    y_test_list, _, pred_list, stds_list, models, station_coordinates, _ = _prepare_data()
    fig, axx = plt.subplots(4, 3, figsize=(14.4, 14.4), dpi=100, sharex='col', sharey='row')

    # we compute the interpolated matrices before
    Z_values = []
    resX, resY = 100, 100
    for model in range(3):
        y_test = y_test_list[model]
        pred = pred_list[model]
        pos_err = np.sqrt((y_test[:, 0] - pred[:, 0]) ** 2 + (y_test[:, 1] - pred[:, 1]) ** 2) * (40075 / 360)
        inner_Z = []
        for i in range(4):
            ind_range = np.where(np.logical_and(y_test[:, 3] > ranges[i][0], y_test[:, 3] < ranges[i][1]))[0]
            X, Y, Z = grid(y_test[:, 1][ind_range], y_test[:, 0][ind_range], pos_err[ind_range], resX=resX, resY=resY)
            Z = ndimage.gaussian_filter(Z, sigma=1.)
            inner_Z.append(Z)
        Z_values.append(inner_Z)

    for model in range(3):

        y_test = y_test_list[model]
        pred = pred_list[model]

        for i in range(4):
            ax = axx[i][model]
            ind_range = np.where(np.logical_and(y_test[:, 3] > ranges[i][0], y_test[:, 3] < ranges[i][1]))[0]
            X, Y, _ = grid(y_test[:, 1][ind_range], y_test[:, 0][ind_range], pos_err[ind_range], resX=resX, resY=resY)

            mapgeo = Basemap(llcrnrlon=np.min(X), llcrnrlat=np.min(Y), urcrnrlon=np.max(X), urcrnrlat=np.max(Y),
                             projection='merc', resolution='i', ax=ax)  # 'c','l','i','h' or 'f'
            mapgeo.drawcoastlines(color='black', linewidth=0.4)  # add coastlines

            if model == 0:
                parallels = np.linspace(np.min(Y), np.max(Y), 6).astype(np.int_)
                mapgeo.drawparallels(parallels, labels=[1, 0, 0, 0], linewidth=0.)

            if i == 3:
                meridians = np.linspace(np.min(X), np.max(X), 4).astype(np.int_)
                mapgeo.drawmeridians(meridians, labels=[0, 0, 0, 1], linewidth=0.)

            X, Y = mapgeo(X, Y)  # transform coordinates
            Sx, Sy = mapgeo(station_coordinates[:, 1], station_coordinates[:, 0])

            norm = matplotlib.colors.Normalize(vmin=np.nanmin(Z_values), vmax=np.nanmax(Z_values))
            # s = ax.contourf(X, Y, Z_values[model][i], cmap='turbo', norm=norm)  # c=cmap(norm(...))

            cmap = matplotlib.cm.get_cmap("turbo").copy()
            cmap.set_bad("white", alpha=0)
            pcm = ax.pcolormesh(X, Y, Z_values[model][i], cmap=cmap, norm=norm, antialiased=True,
                                shading='gouraud', rasterized=True)  # c=cmap(norm(...))

            sm = plt.cm.ScalarMappable(norm=norm, cmap=pcm.cmap)
            sm.set_array([])

            ax.scatter(Sx, Sy, label='stations', c='C3', alpha=0.5, s=16)
            # arrows : base in actual point, head in predicted point
            # spatial quantization : 1x1 degree squares
            # for each square, we compute the average direction and the average length
            # then we make an arrow from the average test position to the average dir,length
            # we assign an event to a square based on the test position (starting point)
            quant_lat = np.linspace(35, 41, 41 - 35 + 1)
            quant_lon = np.linspace(139, 146, 146 - 139 + 1)
            # we store all the variables in order to compute then the maximum length to scale all the arrows by the max
            start_lat_list = []
            start_lon_list = []
            length_list = []
            direction_list = []
            err_lat_list = []
            err_lon_list = []
            for la in range(len(quant_lat)):
                for lo in range(len(quant_lon)):
                    avg_dir = []
                    avg_len = []
                    avg_start_lon = []
                    avg_start_lat = []
                    avg_err_lat_list = []
                    avg_err_lon_list = []
                    for j in range(y_test[:, 1][ind_range].shape[0]):
                        if y_test[:, 0][ind_range][j] > quant_lat[la] and y_test[:, 0][ind_range][j] < quant_lat[
                            la + 1] and y_test[:, 1][ind_range][j] > quant_lon[lo] and y_test[:, 1][ind_range][j] < \
                                quant_lon[lo + 1]:
                            transf_pred_lon, transf_pred_lat = (pred[:, 1][ind_range][j], pred[:, 0][ind_range][j])
                            transf_actual_lon, transf_actual_lat = (
                                y_test[:, 1][ind_range][j], y_test[:, 0][ind_range][j])
                            err_lon = transf_pred_lon - transf_actual_lon
                            err_lat = transf_pred_lat - transf_actual_lat
                            transf_lon, transf_lat = (y_test[:, 1][ind_range][j], y_test[:, 0][ind_range][j])
                            direction = np.arctan2(err_lon, err_lat)  # wrt the north
                            length = np.sqrt(err_lon ** 2 + err_lat ** 2) * 1
                            avg_len.append(length)
                            avg_dir.append(direction)

                            avg_start_lat.append(transf_lat)
                            avg_start_lon.append(transf_lon)
                            avg_err_lat_list.append(err_lat)
                            avg_err_lon_list.append(err_lon)
                    if len(avg_start_lat) > 0:
                        start_lat_list.append(np.mean(avg_start_lat))
                        start_lon_list.append(np.mean(avg_start_lon))
                        length_list.append(np.mean(avg_len))
                        direction_list.append(np.mean(avg_dir))
                        err_lat_list.append(np.mean(avg_err_lat_list))
                        err_lon_list.append(np.mean(avg_err_lon_list))

            mapgeo.quiver(start_lon_list, start_lat_list, length_list * np.sin(direction_list),
                          length_list * np.cos(direction_list), latlon=True, width=.005,
                          facecolor='white', edgecolors='black', linewidths=0.3)

    suptitles_row = ['%.1f $\mathbf{\leq M_w \leq}$ %.1f' % (rngmw[0], rngmw[1]) for _, rngmw in enumerate(ranges)]
    horizontal_pad = 30  # in points
    vertical_pad = 20  # in points
    suptitles_col = models
    for ax, row in zip(axx[:, 0], suptitles_row):
        ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - horizontal_pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size='large', ha='right', va='center', weight='bold')

    for ax, col in zip(axx[0], suptitles_col):
        ax.annotate(col, xy=(0.5, 1), xytext=(0, vertical_pad),
                    xycoords='axes fraction', textcoords='offset points',
                    size='large', ha='center', va='baseline', weight='bold')

    plt.tight_layout()
    cbar = fig.colorbar(sm, ax=axx.ravel().tolist(),
                        pad=0.05)  # , fraction=0.046)#, ticks=np.linspace(min(min_vals), max(max_vals), num=10))
    cbar.ax.set_ylabel('Position error (km)', rotation=270, labelpad=25, size=13)

    plt.savefig('figures/figure6/loc_error2_BM.pdf', format='pdf', bbox_inches='tight')


def figure7():
    y_test_list, depth_test_list, pred_list, stds_list, models, station_coordinates, station_coordinates_full = _prepare_data()
    nearest_station_list = []
    ind_list = []

    for model in range(3):
        y_test = y_test_list[model]
        depth_test = depth_test_list[model]
        nearest_station_actual = np.zeros((y_test.shape[0]))
        for i in range(y_test.shape[0]):
            # compute nearest station
            d_a = euclidean_distances(station_coordinates_full,
                                      np.array([y_test[i, 0], y_test[i, 1], -depth_test[i] * (360 / 40075)]).reshape(-1, 1).T)
            nearest_station_actual[i] = np.min(d_a)
        nearest_station_list.append(nearest_station_actual)
        ind = np.argsort(nearest_station_actual)
        ind_list.append(ind)

    fig, axx = plt.subplots(2, 3, figsize=(14.4, 7.8), dpi=100, sharey='row')

    cmap_type = 'jet'
    mw_ranges = [(5.8, 6.3), (6.3, 6.8), (6.8, 7.5), (7.5, 8.5)]
    cmap = plt.get_cmap(cmap_type)
    N_POINTS = 30
    for model in range(3):
        nearest_station_actual = nearest_station_list[model]
        ind = ind_list[model]
        y_test = y_test_list[model]
        pred = pred_list[model]

        pos_err = np.sqrt((y_test[:, 0] - pred[:, 0]) ** 2 + (y_test[:, 1] - pred[:, 1]) ** 2)
        mw_err = np.abs(y_test[:, 3] - pred[:, 3])

        axx[0][model].scatter(nearest_station_actual[ind], pos_err[ind], c=y_test[:, 3][ind], s=10, alpha=0.5,
                              cmap=cmap_type)
        axx[1][model].scatter(nearest_station_actual[ind], mw_err[ind], c=y_test[:, 3][ind], s=10, alpha=0.5,
                              cmap=cmap_type)
        inv_sc_pos = axx[0][model].scatter(nearest_station_actual[ind], pos_err[ind], c=y_test[:, 3][ind], s=0, alpha=1,
                                           cmap=cmap_type)  # dirty
        inv_sc_mw = axx[1][model].scatter(nearest_station_actual[ind], mw_err[ind], c=y_test[:, 3][ind], s=0, alpha=1,
                                          cmap=cmap_type)  # dirty

        for k, rng in enumerate(mw_ranges):
            ind_rng_mw = np.where(np.logical_and(y_test[:, 3][ind] >= rng[0], y_test[:, 3][ind] < rng[1]))[0]
            median_curve_pos = []
            median_curve_mw = []
            histvals, binedges = np.histogram(nearest_station_actual[ind], bins=N_POINTS)
            xvals = nearest_station_actual[ind][ind_rng_mw]
            for i in range(len(binedges) - 1):
                ind_bin = np.where(np.logical_and(xvals >= binedges[i], xvals < binedges[i + 1]))[0]
                if len(ind_bin) > 0:
                    median_curve_mw.append(np.percentile(mw_err[ind][ind_rng_mw][ind_bin], 50))
                    median_curve_pos.append(np.percentile(pos_err[ind][ind_rng_mw][ind_bin], 50))
            color = int(255 * ((rng[1] + rng[0]) / 2 - 5.8) / (8.5 - 5.8))

            axx[0][model].plot(binedges[:-1], ndimage.gaussian_filter1d(median_curve_pos, sigma=5),
                               label='$M_w \in (%.1f, %.1f)$' % (rng[0], rng[1]), linewidth=2, color=cmap(color))
            axx[1][model].plot(binedges[:-1], ndimage.gaussian_filter1d(median_curve_mw, sigma=5),
                               label='$M_w \in (%.1f, %.1f)$' % (rng[0], rng[1]), linewidth=2, color=cmap(color))

            if model == 2:
                axx[0][model].legend(bbox_to_anchor=(1.5, 1.5))

        axx[1][model].set_xlabel('Euclidean distance to nearest station [°]')

        axx[0][model].axvline(x=0.5, linestyle='--', zorder=-10000)
        axx[1][model].axvline(x=0.5, linestyle='--', zorder=-10000)

        axx[0][model].axvline(x=3, linestyle='--', zorder=-10000)
        axx[1][model].axvline(x=3, linestyle='--', zorder=-10000)

        if model == 0:
            axx[0][model].set_ylabel('Position error [°]')
            axx[1][model].set_ylabel('Magnitude error')

        for k, ax in enumerate(axx.flatten()[:3]):
            ax.set_title(models[k], fontweight='bold')

    for ax in axx.reshape(-1):
        ax.yaxis.set_tick_params(labelbottom=True)
        # plt.setp(ax.get_yticklabels(), visible=True)

    cbar = fig.colorbar(inv_sc_mw, ax=axx.ravel().tolist())
    cbar.ax.set_ylabel('Actual magnitude', rotation=270, labelpad=15)

    plt.savefig('figures/figure7/err_nearest_station.pdf', format='pdf', bbox_inches='tight')
    plt.close(fig)


def figure8():
    with np.load('./pred/pred_TRANSF_determ.npz') as f:
        y_pred, _, depth_test, y_test, _, _ = f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f[
            'err_loc']

    pos_err = np.sqrt((y_test[:, 0] - y_pred[:, 0]) ** 2 + (y_test[:, 1] - y_pred[:, 1]) ** 2) * (40075 / 360)
    ind_zones = _get_zone_idx(y_test)
    fig, axes = plt.subplots(3, 3, figsize=(14.4, 7.8), dpi=100, sharex='col', sharey='row')
    zones2 = ['near', 'interm.', 'far']
    depths = [(0, 30), (30, 60), (60, 100)]
    for z, zone in enumerate(ind_zones):
        for d, drng in enumerate(depths):
            ind_depth = np.where(np.logical_and(depth_test[zone] >= drng[0], depth_test[zone] < drng[1]))[0]
            ind_sorted_mw = np.argsort(y_test[:, 3][zone][ind_depth])
            axes[z][d].plot(y_test[:, 3][zone][ind_depth][ind_sorted_mw], pos_err[zone][ind_depth][ind_sorted_mw],
                            linewidth=0.8)
            axes[z][d].plot(y_test[:, 3][zone][ind_depth][ind_sorted_mw],
                            medfilt(pos_err[zone][ind_depth][ind_sorted_mw], 15), linewidth=2, label='median')
            axes[z][d].set_ylabel('km')
            axes[z][d].set_xlabel('$M_w$')
            # axes[d][z].set_title('Pos. err. (%s field), depth $\in (%d, %d)$' % (zones2[z], drng[0], drng[1]))
            axes[z][d].legend()

    horizontal_pad = 10  # in points
    vertical_pad = 20  # in points
    suptitles_row = ['{0} field'.format(zone) for zone in zones2]

    suptitles_col = ['depth $\mathbf{\leq}$ 30 km', '30 km $\mathbf{<}$ depth $\mathbf{\leq}$ 60 km',
                     '60 km $\mathbf{<}$ depth $\mathbf{\leq}$ 100 km']

    for ax, col in zip(axes[0], suptitles_col):
        ax.annotate(col, xy=(0.5, 1), xytext=(0, vertical_pad),
                    xycoords='axes fraction', textcoords='offset points',
                    size='large', ha='center', va='baseline', weight='bold')

    for ax, row in zip(axes[:, 0], suptitles_row):
        ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - horizontal_pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size='large', ha='right', va='center', weight='bold')

    for axis in axes.reshape(-1):
        axis.yaxis.set_tick_params(labelbottom=True)
        axis.xaxis.set_tick_params(labelbottom=True)

    plt.tight_layout()
    fig.subplots_adjust(left=0.15, top=0.95)

    plt.savefig('figures/figure8/TRA_pos_err_all_samples.pdf', format='pdf',
                bbox_inches='tight')
    # plt.show()
    plt.close(fig)


def figure9():
    with np.load('./pred/pred_TRANSF_determ.npz') as f:
        y_pred, _, _, y_test, _, _ = f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    ind_zones = _get_zone_idx(y_test)
    zones2 = ['near', 'interm.', 'far']
    ranges = [(5.8, 6.1), (6.1, 6.4), (6.4, 7.5), (7.5, 8.5)]

    fig, axes = plt.subplots(3, 4, figsize=(14.4, 7.8), dpi=100, sharex='col')
    for i, zone in enumerate(ind_zones):
        for j, rngmw in enumerate(ranges):
            curr_range_for_zone = np.where(np.logical_and(y_test[zone, 3] > rngmw[0], y_test[zone, 3] < rngmw[1]))[0]

            axes[i][j].hist([y_test[zone, 3][curr_range_for_zone], y_pred[zone, 3][curr_range_for_zone]])

            axes[i][j].legend(['actual', 'pred'])
            # axes[i][j].set_title('Mw distrib (%s), $M_w \in (%.1f, %.1f)$' % (zones[i], rngmw[0], rngmw[1]))
            if i == 2:
                axes[i][j].set_xlabel('$M_w$')
            if j == 0:
                axes[i][j].set_ylabel('n. occurrences')

    horizontal_pad = 10  # in points
    vertical_pad = 20  # in points
    suptitles_row = ['{0} field'.format(zone) for zone in zones2]
    # suptitles_col = ['depth $\mathbf{\in (%d, %d)}$' % (drng[0], drng[1]) for _, drng in enumerate(depths)]

    suptitles_col = ['%.1f $\mathbf{\leq M_w \leq}$ %.1f' % (rngmw[0], rngmw[1]) for _, rngmw in enumerate(ranges)]

    for ax, col in zip(axes[0], suptitles_col):
        ax.annotate(col, xy=(0.5, 1), xytext=(0, vertical_pad),
                    xycoords='axes fraction', textcoords='offset points',
                    size='large', ha='center', va='baseline', weight='bold')

    for ax, row in zip(axes[:, 0], suptitles_row):
        ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - horizontal_pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size='large', ha='right', va='center', weight='bold')

    # plt.suptitle('Retrieved parameter distributions for Mw ranges and zones')
    plt.tight_layout()
    plt.savefig('figures/figure9/TRA_hist_retrieved_zones.pdf', format='pdf', bbox_inches='tight')
    # plt.show()
    plt.close(fig)


def figure10():
    with np.load('geo_data/dataset_gamit.npz', allow_pickle=True) as f:
        X_gamit, y_gamit, dates_gamit, fm_gamit, nodal_gamit = f['X'], f['y'], f['dates'], f['fm'], f['nodal']
    with np.load('geo_data/dataset_ngl.npz', allow_pickle=True) as f:
        X_ngl, y_ngl, dates_ngl, fm_ngl, nodal_ngl = f['X'], f['y'], f['dates'], f['fm'], f['nodal']
    catalogs = [y_gamit, y_ngl]
    fmecs = [fm_gamit, fm_ngl]
    nodals = [nodal_gamit, nodal_ngl]
    dataset_type = ['isterre_dd', 'ngl_ppp']

    for d, dset in enumerate(dataset_type):
        catalog = catalogs[d]
        region = [139.5 - 1, 145 + 1, 35 - 1, 41 + 1]

        fig = pygmt.Figure()
        fig.basemap(region=region, projection="M15c", frame=True)
        fig.coast(land="lightgrey", water="skyblue")

        magnitude_scaled = _magnitude_scaling(catalog[:, 3])

        # pygmt.makecpt(series=[min([d.year for d in dates]), max([d.year for d in dates])])
        # fig.plot(x=143, y=40, sizes=[20], label='alabel')
        for i, fm in enumerate(fmecs[d]):
            # print(catalog[i, 3])
            focal_mechanism = dict(strike=int(fm[0]), dip=int(fm[1]), rake=int(fm[2]), magnitude=magnitude_scaled[i])
            fig.meca(focal_mechanism, scale="1c", longitude=catalog[i][1], latitude=catalog[i][0], depth=catalog[i][2],
                     G=characterizable_thrust_color(catalog[i], fmecs[d][i], nodals[d][i], color_type='non_rgb'))

        fig.legend('geo_data/legend_fm_2.txt', position="JTL+jTL+o0.5c", box="+gwhite+p1p")

        fig.meca(dict(strike=180, dip=20, rake=90, magnitude=_magnitude_scaling(8)), scale="1c", longitude=139.29,
                 latitude=41.45, depth=10.)
        fig.meca(dict(strike=180, dip=20, rake=90, magnitude=_magnitude_scaling(7)), scale="1c", longitude=139.29,
                 latitude=40.8, depth=10.)
        fig.meca(dict(strike=180, dip=20, rake=90, magnitude=_magnitude_scaling(6)), scale="1c", longitude=139.29,
                 latitude=40.15, depth=10.)

        # fig.colorbar(frame='af+l"Depth (km)"')

        # fig.show(method='external')
        # fig.savefig('plots/common/real/NEW/catalog_gamit.png', transparent=True)
        fig.savefig(f'figures/figure10/map_characterizable_{dset}.pdf')


def figure11():
    with np.load('geo_data/dataset_gamit.npz', allow_pickle=True) as f:
        X_gamit, y_gamit, dates_gamit, fm_gamit, nodal_gamit = f['X'], f['y'], f['dates'], f['fm'], f['nodal']
    with np.load('geo_data/dataset_ngl.npz', allow_pickle=True) as f:
        X_ngl, y_ngl, dates_ngl, fm_ngl, nodal_ngl = f['X'], f['y'], f['dates'], f['fm'], f['nodal']

    with np.load('pred/RESULTS_real_TS_gamit.npz') as f:
        pred_TS_gamit, stds_TS_gamit, depth_test_TS_gamit, y_test_TS_gamit, err_mw_TS_gamit, err_loc_TS_gamit = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_TS_ngl.npz') as f:
        pred_TS_ngl, stds_TS_ngl, depth_test_TS_ngl, y_test_TS_ngl, err_mw_TS_ngl, err_loc_TS_ngl = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_IMG_gamit.npz') as f:
        pred_IMG_gamit, stds_IMG_gamit, depth_test_IMG_gamit, y_test_IMG_gamit, err_mw_IMG_gamit, err_loc_IMG_gamit = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_IMG_ngl.npz') as f:
        pred_IMG_ngl, stds_IMG_ngl, depth_test_IMG_ngl, y_test_IMG_ngl, err_mw_IMG_ngl, err_loc_IMG_ngl = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_TRA_gamit.npz') as f:
        pred_TRA_gamit, stds_TRA_gamit, depth_test_TRA_gamit, y_test_TRA_gamit, err_mw_TRA_gamit, err_loc_TRA_gamit = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_TRA_ngl.npz') as f:
        pred_TRA_ngl, stds_TRA_ngl, depth_test_TRA_ngl, y_test_TRA_ngl, err_mw_TRA_ngl, err_loc_TRA_ngl = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    catalogs = [y_gamit, y_ngl]  # characterizable events only
    fmecs = [fm_gamit, fm_ngl]
    nodals = [nodal_gamit, nodal_ngl]
    dates_all = [dates_gamit, dates_ngl]
    displacements = [X_gamit, X_ngl]
    predictions_all = [[pred_TS_gamit, pred_IMG_gamit, pred_TRA_gamit], [pred_TS_ngl, pred_IMG_ngl, pred_TRA_ngl]]

    models = ['TS', 'IMG', 'TRA']
    dataset_type = ['isterre_dd', 'ngl_ppp']

    for d, dset in enumerate(dataset_type):
        catalog = catalogs[d]
        dates = dates_all[d]
        displacement = displacements[d]
        predictions = predictions_all[d]
        fmec = fmecs[d]
        char_lbls = characterizable_catalog(catalog)
        char_colors = characterizable_colors(char_lbls)
        ind_thrust_char, ind_not_thrust_char = thrust_indices(fmec[char_lbls], nodals[d][char_lbls])

        fig, axes = plt.subplots(3, 3, figsize=(10.8, 7.8), dpi=100)

        num_dates = np.sum(char_lbls)

        cmap = plt.get_cmap('jet', num_dates)
        dates_code_colormap = {x: '{0} {1} {2}'.format(date.day, date.strftime('%B'), date.year) for x, date in
                               enumerate(dates[char_lbls])}
        ind_tohoku = list(dates_code_colormap.keys())[list(dates_code_colormap.values()).index('11 March 2011')]
        dates_code_colormap[ind_tohoku] += ' (Tohoku)'
        dates_dict = {v: k for k, v in dates_code_colormap.items()}  # crea direttamente al contrario
        dates_colormap_ind = list(dates_code_colormap.keys())

        for model in range(3):
            char_labels = char_lbls.copy()
            ind_thrust_chr = ind_thrust_char.copy()
            dates_chr = dates_dict.copy()
            # print(dates_chr);print(ind_thrust_chr);exit(0)
            if model == 0:
                if dset == 'isterre_dd':
                    char_labels[27] = False
                    char_labels[36] = False
                    # char_labels[28] = False  # added 19 July 2008
                    ind_thrust_chr = np.delete(ind_thrust_chr,
                                               (3, 4))  # 13 june 2008 and tohoku 3-4th element in the characterizable
                    # ind_thrust_chr = np.delete(ind_thrust_chr, (3, 4, 5))  # added 19 July 2008
                    del dates_chr['13 June 2008']
                    # del dates_chr['19 July 2008']
                else:
                    char_labels[5] = False
                    ind_thrust_chr = np.delete(ind_thrust_chr, 0)  # tohoku 1st element in the characterizable
                del dates_chr['11 March 2011 (Tohoku)']
            print('char_labels:', char_labels)
            num_characterizable = np.count_nonzero(char_labels)
            print("num_characterizable:", num_characterizable)
            markers_char = np.array(['o' for _ in range(num_characterizable)])
            print('ind_thrust_chr', ind_thrust_chr)
            markers_char[ind_thrust_chr] = 'x'

            pred = predictions[model]
            # pred_stds = stds_list[model]
            minlatitude = np.minimum(catalog[:, 0][char_labels].min(), pred[:, 0][char_labels].min())
            maxlatitude = np.maximum(catalog[:, 0][char_labels].max(), pred[:, 0][char_labels].max())
            minlongitude = np.minimum(catalog[:, 1][char_labels].min(), pred[:, 1][char_labels].min())
            maxlongitude = np.maximum(catalog[:, 1][char_labels].max(), pred[:, 1][char_labels].max())
            minmag = np.minimum(catalog[:, 3][char_labels].min(), pred[:, 3][char_labels].min())
            maxmag = np.maximum(catalog[:, 3][char_labels].max(), pred[:, 3][char_labels].max())
            var_lims = ((minlatitude, maxlatitude), (minlongitude, maxlongitude), (minmag, maxmag))
            mean_preds = [pred[:, 0], pred[:, 1], pred[:, 3]]
            # mean_stds = [pred_stds[:, 0], pred_stds[:, 1], pred_stds[:, 2]]
            actualvals = [catalog[:, 0], catalog[:, 1], catalog[:, 3]]
            titles = ['Latitude (degrees)', 'Longitude (degrees)', 'Magnitude']
            mw_ranges = [(5.8, 6.3), (6.3, 6.8), (6.8, 7.5), (7.5, 8.5)]

            for expnum in range(3):
                axes[expnum][model].plot(var_lims[expnum], var_lims[expnum], "k--")
                # legend (it will be replaced by the correct one)

                # _ = axes[expnum][model].scatter(actualvals[expnum][char_labels][ind_thrust_chr][0], mean_preds[expnum][char_labels][ind_thrust_chr][0],
                _ = axes[expnum][model].scatter(actualvals[expnum][char_labels][ind_thrust_chr],
                                                mean_preds[expnum][char_labels][ind_thrust_chr],
                                                alpha=1, c='C0', marker='x', label='thrust')
                # _ = axes[expnum][model].scatter(actualvals[expnum][char_labels][ind_not_thrust_char][0], mean_preds[expnum][char_labels][ind_not_thrust_char][0],
                #                                alpha=1, c='C0', marker='x', label='non_thrust')
                # norm = Normalize(vmin=np.min(dates_as_float[char_labels]), vmax=np.max(dates_as_float[char_labels]))

                # for i in range(num_dates):
                for i, date_key in enumerate(dates_chr):
                    print('dates:')
                    print(i, date_key)
                    print('colormap indicizzata con:', dates_chr[date_key])
                    _ = axes[expnum][model].scatter(actualvals[expnum][char_labels][i],
                                                    mean_preds[expnum][char_labels][i],
                                                    alpha=1, color=cmap(dates_chr[date_key]), cmap=cmap,
                                                    marker=markers_char[i])

                if model != 0:
                    sc = axes[expnum][model].scatter(actualvals[expnum][char_labels], mean_preds[expnum][char_labels],
                                                     alpha=1, c=dates_colormap_ind, cmap=cmap, s=0)  # super dirty
                if expnum == 2:
                    axes[expnum][model].set_xlabel("Actual value")
                if model == 0:
                    axes[expnum][model].set_ylabel("Predicted value")
                axes[expnum][model].set(adjustable='box', aspect='equal')

        for axis in axes.reshape(-1):
            axis.yaxis.set_tick_params(labelbottom=True)

        plt.tight_layout()
        cbar = fig.colorbar(sc, ax=axes.ravel().tolist())
        # cbar.ax.set_ylabel('Year', rotation=270, labelpad=55)

        # set ticks locations (not very elegant, but it works):
        # - shift by 0.5
        # - scale so that the last value is at the center of the last color
        tick_locs = (np.arange(num_dates) + 0.5) * (num_dates - 1) / num_dates
        cbar.set_ticks(tick_locs)

        # set tick labels (as before)
        cbar.set_ticklabels(list(dates_code_colormap.values()))

        horizontal_pad = 10  # in points
        vertical_pad = 20  # in points
        suptitles_row = ['Latitude [°]', 'Longitude [°]', 'Magnitude']
        suptitles_col = models

        for ax, col in zip(axes[0], suptitles_col):
            ax.annotate(col, xy=(0.5, 1), xytext=(0, vertical_pad),
                        xycoords='axes fraction', textcoords='offset points',
                        size='large', ha='center', va='baseline', weight='bold')

        for ax, row in zip(axes[:, 0], suptitles_row):
            ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - horizontal_pad, 0),
                        xycoords=ax.yaxis.label, textcoords='offset points',
                        size='large', ha='right', va='center', weight='bold')

        plt.savefig(f'figures/figure11/{dset}/actual_predicted_char_char_only_cbar2.pdf', format='pdf',
                    bbox_inches='tight')
        plt.close(fig)


def figure12():
    with np.load('geo_data/dataset_gamit.npz', allow_pickle=True) as f:
        X_gamit, y_gamit, dates_gamit, fm_gamit, nodal_gamit = f['X'], f['y'], f['dates'], f['fm'], f['nodal']
    with np.load('geo_data/dataset_ngl.npz', allow_pickle=True) as f:
        X_ngl, y_ngl, dates_ngl, fm_ngl, nodal_ngl = f['X'], f['y'], f['dates'], f['fm'], f['nodal']

    with np.load('pred/RESULTS_real_TS_gamit.npz') as f:
        pred_TS_gamit, stds_TS_gamit, depth_test_TS_gamit, y_test_TS_gamit, err_mw_TS_gamit, err_loc_TS_gamit = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_TS_ngl.npz') as f:
        pred_TS_ngl, stds_TS_ngl, depth_test_TS_ngl, y_test_TS_ngl, err_mw_TS_ngl, err_loc_TS_ngl = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_IMG_gamit.npz') as f:
        pred_IMG_gamit, stds_IMG_gamit, depth_test_IMG_gamit, y_test_IMG_gamit, err_mw_IMG_gamit, err_loc_IMG_gamit = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_IMG_ngl.npz') as f:
        pred_IMG_ngl, stds_IMG_ngl, depth_test_IMG_ngl, y_test_IMG_ngl, err_mw_IMG_ngl, err_loc_IMG_ngl = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_TRA_gamit.npz') as f:
        pred_TRA_gamit, stds_TRA_gamit, depth_test_TRA_gamit, y_test_TRA_gamit, err_mw_TRA_gamit, err_loc_TRA_gamit = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_TRA_ngl.npz') as f:
        pred_TRA_ngl, stds_TRA_ngl, depth_test_TRA_ngl, y_test_TRA_ngl, err_mw_TRA_ngl, err_loc_TRA_ngl = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    characterizable_idx = [characterizable_catalog(y_gamit), characterizable_catalog(y_ngl)]
    catalogs = [y_gamit[characterizable_idx[0]], y_ngl[characterizable_idx[1]]]  # characterizable events only
    fmecs = [fm_gamit[characterizable_idx[0]], fm_ngl[characterizable_idx[1]]]
    nodals = [nodal_gamit[characterizable_idx[0]], nodal_ngl[characterizable_idx[1]]]
    dates_all = [dates_gamit[characterizable_idx[0]], dates_ngl[characterizable_idx[1]]]
    displacements = [X_gamit[characterizable_idx[0]], X_ngl[characterizable_idx[1]]]
    predictions_all = [[pred_TS_gamit[characterizable_idx[0]], pred_IMG_gamit[characterizable_idx[0]],
                        pred_TRA_gamit[characterizable_idx[0]]],
                       [pred_TS_ngl[characterizable_idx[1]], pred_IMG_ngl[characterizable_idx[1]],
                        pred_TRA_ngl[characterizable_idx[1]]]]

    dataset_type = ['isterre_dd', 'ngl_ppp']

    for d, dset in enumerate(dataset_type):
        catalog = catalogs[d]
        dates = dates_all[d]
        displacement = displacements[d]
        predictions = predictions_all[d]
        fmec = fmecs[d]
        _, station_coordinates = honshu_coordinates()
        region = [139 - 1, 144 + 1, 35 - 1, 41 + 1]
        scale = 0.1

        with pygmt.clib.Session() as session:
            session.call_module('gmtset', 'FONT 16p')
            session.call_module('gmtset', 'FONT_TITLE 20p Helvetica-Bold')

        for i in range(len(catalog)):
            fig = pygmt.Figure()
            fig.basemap(region=region, projection="M15c", frame=["a",
                                                                 '+t"{0} {1} {2},    Mw:  {3},    depth:  {4} km"'.format(
                                                                     dates[i].day, dates[i].strftime("%B"),
                                                                     dates[i].year,
                                                                     catalog[i, 3], catalog[i, 2])])
            fig.coast(land="lightgrey", water="skyblue")
            datax = displacement[i, :, 51, 1] - displacement[i, :, 49, 1]
            datay = displacement[i, :, 51, 0] - displacement[i, :, 49, 0]
            L = np.sqrt(datax ** 2 + datay ** 2)

            # mask "outlier" values (< 3*std)
            std = np.nanstd(L)
            outlier_mask = L > 3 * std
            L_copy = L.copy()
            L_copy[outlier_mask] = np.nan
            L_max = np.nanmax(L_copy)

            # now clip outliers
            # L[outlier_mask] = 3 * std

            mm_unit = 2

            A = np.arctan2(datay, datax) * 180 / np.pi  # rispetto a orizzontale

            print(A.shape, L.shape)

            x0 = station_coordinates[:, 1]
            y0 = station_coordinates[:, 0]
            fig.plot(x=x0, y=y0, style="v0.3c+e", direction=[A, (L / L_max) * mm_unit], pen="2p", color="black")

            # fig.plot(x=144, y=34.5, style="v0.6c+e", direction=[[0], [mm_unit]], pen="2p", color="black")
            # fig.text(text='{0} mm'.format(ceil(L_max)), x=144.5, y=34.4)
            fig.plot(x=143., y=35., style="v0.6c+e", direction=[[0], [mm_unit]], pen="2p", color="black")
            fig.text(text='{0} mm'.format(math.ceil(L_max)), x=143.45, y=34.85)
            focal_mechanism = dict(strike=int(fmec[i][0]), dip=int(fmec[i][1]), rake=int(fmec[i][2]),
                                   magnitude=float(catalog[i][3]))
            fig.meca(focal_mechanism, scale="1c", longitude=catalog[i, 1], latitude=catalog[i, 0], depth=catalog[i, 2])

            fig.plot(x=predictions[0][i, 1], y=predictions[0][i, 0], size=[predictions[0][i, 3] * scale],
                     color='yellow',
                     style="cc", pen="black", label='"TS+S0.25c"')
            fig.plot(x=predictions[1][i, 1], y=predictions[1][i, 0], size=[predictions[1][i, 3] * scale], color='brown',
                     style="cc", pen="black", label='"IMG+S0.25c"')
            fig.plot(x=predictions[2][i, 1], y=predictions[2][i, 0], size=[predictions[2][i, 3] * scale],
                     color='dodgerblue', style="cc", pen="black", label='"TRA+S0.25c"')

            # fig.plot(x=141.4, y=34.5, style="v0.6c+e", direction=[[0], [L_max / 3]], pen="2p", color="black")
            # fig.text(text='{0} mm'.format(ceil(L_max / 3)), x=141.8, y=34.4)

            fig.legend()
            # fig.show(method='external')
            fig.savefig(f'figures/figure12/{dset}/disp_field_{dates[i].year}_{dates[i].month}_{dates[i].day}.pdf')


def statistics_real_data():
    with np.load('geo_data/dataset_gamit.npz', allow_pickle=True) as f:
        X_gamit, y_gamit, dates_gamit, fm_gamit, nodal_gamit = f['X'], f['y'], f['dates'], f['fm'], f['nodal']
    with np.load('geo_data/dataset_ngl.npz', allow_pickle=True) as f:
        X_ngl, y_ngl, dates_ngl, fm_ngl, nodal_ngl = f['X'], f['y'], f['dates'], f['fm'], f['nodal']

    with np.load('pred/RESULTS_real_TS_gamit.npz') as f:
        pred_TS_gamit, stds_TS_gamit, depth_test_TS_gamit, y_test_TS_gamit, err_mw_TS_gamit, err_loc_TS_gamit = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_TS_ngl.npz') as f:
        pred_TS_ngl, stds_TS_ngl, depth_test_TS_ngl, y_test_TS_ngl, err_mw_TS_ngl, err_loc_TS_ngl = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_IMG_gamit.npz') as f:
        pred_IMG_gamit, stds_IMG_gamit, depth_test_IMG_gamit, y_test_IMG_gamit, err_mw_IMG_gamit, err_loc_IMG_gamit = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_IMG_ngl.npz') as f:
        pred_IMG_ngl, stds_IMG_ngl, depth_test_IMG_ngl, y_test_IMG_ngl, err_mw_IMG_ngl, err_loc_IMG_ngl = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_TRA_gamit.npz') as f:
        pred_TRA_gamit, stds_TRA_gamit, depth_test_TRA_gamit, y_test_TRA_gamit, err_mw_TRA_gamit, err_loc_TRA_gamit = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('pred/RESULTS_real_TRA_ngl.npz') as f:
        pred_TRA_ngl, stds_TRA_ngl, depth_test_TRA_ngl, y_test_TRA_ngl, err_mw_TRA_ngl, err_loc_TRA_ngl = \
            f['pred'], f['stds'], f['depth_test'], f['y_test'], f['err_mw'], f['err_loc']

    with np.load('geo_data/gamit_data.npz', allow_pickle=True) as f:
        time_series_gamit, images_gamit, image_time_series_gamit = f['time_series'], f['images'], f['image_time_series']
    with np.load('geo_data/ngl_data.npz', allow_pickle=True) as f:
        time_series_ngl, images_ngl, image_time_series_ngl = f['time_series'], f['images'], f['image_time_series']

    # print(time_series_gamit.shape)
    # print(time_series_ngl.shape)

    #####################################################################################
    with np.load('geo_data/time_series_gamit.npz', allow_pickle=True) as f:
        ts_gamit, t_gamit = f['data'], f['t']

    with np.load('geo_data/time_series_ngl.npz', allow_pickle=True) as f:
        ts_ngl, t_ngl = f['data'], f['t']

    char_gamit = characterizable_catalog(y_gamit)
    char_ngl = characterizable_catalog(y_ngl)

    '''print('isterre/dd')
    print(dates_gamit[char_gamit])
    print(y_test_TS_gamit[char_gamit])
    print(pred_TS_gamit[char_gamit])
    print('ngl/ppp')
    print(dates_ngl[char_ngl])
    print(y_test_TS_ngl[char_ngl])
    print(pred_TS_ngl[char_ngl])'''

    err_loc_TS_gamit = np.sqrt((y_gamit[:, 0][char_gamit] - pred_TS_gamit[:, 0][char_gamit]) ** 2 + (
            y_gamit[:, 1][char_gamit] - pred_TS_gamit[:, 1][char_gamit]) ** 2) * (40075 / 360)
    err_loc_IMG_gamit = np.sqrt((y_gamit[:, 0][char_gamit] - pred_IMG_gamit[:, 0][char_gamit]) ** 2 + (
            y_gamit[:, 1][char_gamit] - pred_IMG_gamit[:, 1][char_gamit]) ** 2) * (40075 / 360)
    err_loc_TRA_gamit = np.sqrt((y_gamit[:, 0][char_gamit] - pred_TRA_gamit[:, 0][char_gamit]) ** 2 + (
            y_gamit[:, 1][char_gamit] - pred_TRA_gamit[:, 1][char_gamit]) ** 2) * (40075 / 360)

    err_loc_TS_ngl = np.sqrt((y_ngl[:, 0][char_ngl] - pred_TS_ngl[:, 0][char_ngl]) ** 2 + (
            y_ngl[:, 1][char_ngl] - pred_TS_ngl[:, 1][char_ngl]) ** 2) * (40075 / 360)
    err_loc_IMG_ngl = np.sqrt((y_ngl[:, 0][char_ngl] - pred_IMG_ngl[:, 0][char_ngl]) ** 2 + (
            y_ngl[:, 1][char_ngl] - pred_IMG_ngl[:, 1][char_ngl]) ** 2) * (40075 / 360)
    err_loc_TRA_ngl = np.sqrt((y_ngl[:, 0][char_ngl] - pred_TRA_ngl[:, 0][char_ngl]) ** 2 + (
            y_ngl[:, 1][char_ngl] - pred_TRA_ngl[:, 1][char_ngl]) ** 2) * (40075 / 360)

    err_mw_TS_gamit = np.abs(y_gamit[:, 3][char_gamit] - pred_TS_gamit[:, 3][char_gamit])
    err_mw_IMG_gamit = np.abs(y_gamit[:, 3][char_gamit] - pred_IMG_gamit[:, 3][char_gamit])
    err_mw_TRA_gamit = np.abs(y_gamit[:, 3][char_gamit] - pred_TRA_gamit[:, 3][char_gamit])

    err_mw_TS_ngl = np.abs(y_ngl[:, 3][char_ngl] - pred_TS_ngl[:, 3][char_ngl])
    err_mw_IMG_ngl = np.abs(y_ngl[:, 3][char_ngl] - pred_IMG_ngl[:, 3][char_ngl])
    err_mw_TRA_ngl = np.abs(y_ngl[:, 3][char_ngl] - pred_TRA_ngl[:, 3][char_ngl])

    print('Statistics on all characterizable events (mean ± std:')

    dataset_type = ['ISTerre/DD', 'NGL/PPP']
    table = PrettyTable(['Model', f'Position error (km) [{dataset_type[0]}]', f'Magnitude error [{dataset_type[0]}]',
                         f'Position error (km) [{dataset_type[1]}]', f'Magnitude error [{dataset_type[1]}]'])
    table.add_row(['TS', f'{err_loc_TS_gamit.mean():.2f} ± {err_loc_TS_gamit.std():.2f}',
                   f'{err_mw_TS_gamit.mean():.2f} ± {err_mw_TS_gamit.std():.2f}',
                   f'{err_loc_TS_ngl.mean():.2f} ± {err_loc_TS_ngl.std():.2f}',
                   f'{err_mw_TS_ngl.mean():.2f} ± {err_mw_TS_ngl.std():.2f}'])
    table.add_row(['IMG', f'{err_loc_IMG_gamit.mean():.2f} ± {err_loc_IMG_gamit.std():.2f}',
                   f'{err_mw_IMG_gamit.mean():.2f} ± {err_mw_IMG_gamit.std():.2f}',
                   f'{err_loc_IMG_ngl.mean():.2f} ± {err_loc_IMG_ngl.std():.2f}',
                   f'{err_mw_IMG_ngl.mean():.2f} ± {err_mw_IMG_ngl.std():.2f}'])
    table.add_row(['TRA', f'{err_loc_TRA_gamit.mean():.2f} ± {err_loc_TRA_gamit.std():.2f}',
                   f'{err_mw_TRA_gamit.mean():.2f} ± {err_mw_TRA_gamit.std():.2f}',
                   f'{err_loc_TRA_ngl.mean():.2f} ± {err_loc_TRA_ngl.std():.2f}',
                   f'{err_mw_TRA_ngl.mean():.2f} ± {err_mw_TRA_ngl.std():.2f}'])

    print(table)

    print('Statistics on all characterizable events (median ± MAD:')

    dataset_type = ['ISTerre/DD', 'NGL/PPP']
    table = PrettyTable(['Model', f'Position error (km) [{dataset_type[0]}]', f'Magnitude error [{dataset_type[0]}]',
                         f'Position error (km) [{dataset_type[1]}]', f'Magnitude error [{dataset_type[1]}]'])
    table.add_row(['TS', f'{np.median(err_loc_TS_gamit):.2f} ± {median_abs_deviation(err_loc_TS_gamit):.2f}',
                   f'{np.median(err_mw_TS_gamit):.2f} ± {median_abs_deviation(err_mw_TS_gamit):.2f}',
                   f'{np.median(err_loc_TS_ngl):.2f} ± {median_abs_deviation(err_loc_TS_ngl):.2f}',
                   f'{np.median(err_mw_TS_ngl):.2f} ± {median_abs_deviation(err_mw_TS_ngl):.2f}'])
    table.add_row(['IMG', f'{np.median(err_loc_IMG_gamit):.2f} ± {median_abs_deviation(err_loc_IMG_gamit):.2f}',
                   f'{np.median(err_mw_IMG_gamit):.2f} ± {median_abs_deviation(err_mw_IMG_gamit):.2f}',
                   f'{np.median(err_loc_IMG_ngl):.2f} ± {median_abs_deviation(err_loc_IMG_ngl):.2f}',
                   f'{np.median(err_mw_IMG_ngl):.2f} ± {median_abs_deviation(err_mw_IMG_ngl):.2f}'])
    table.add_row(['TRA', f'{np.median(err_loc_TRA_gamit):.2f} ± {median_abs_deviation(err_loc_TRA_gamit):.2f}',
                   f'{np.median(err_mw_TRA_gamit):.2f} ± {median_abs_deviation(err_mw_TRA_gamit):.2f}',
                   f'{np.median(err_loc_TRA_ngl):.2f} ± {median_abs_deviation(err_loc_TRA_ngl):.2f}',
                   f'{np.median(err_mw_TRA_ngl):.2f} ± {median_abs_deviation(err_mw_TRA_ngl):.2f}'])

    print(table)

    print('Statistics on thrust characterizable events only:')
    # ind_thrust, ind_not_thrust = thrust_indices(fmec, aux_fmec)  # all events
    char_lbls_gamit = characterizable_catalog(y_gamit)
    char_lbls_ngl = characterizable_catalog(y_ngl)

    '''print('catalog ISTerre/DD:')
    for i in range(len(fm_gamit[char_lbls_gamit])):
        print(fm_gamit[char_lbls_gamit][i], 'thrust' if isthrust(fm_gamit[char_lbls_gamit][i], nodal_gamit[char_lbls_gamit][i]) else 'non-thrust', f'lat: {y_gamit[char_lbls_gamit][i][0]}, lon: {y_gamit[char_lbls_gamit][i][1]}')
    print('catalog NGL/PPP:')
    for i in range(len(fm_ngl[char_lbls_ngl])):
        print(fm_ngl[char_lbls_ngl][i], 'thrust' if isthrust(fm_ngl[char_lbls_ngl][i], nodal_ngl[char_lbls_ngl][i]) else 'non-thrust', f'lat: {y_ngl[char_lbls_ngl][i][0]}, lon: {y_ngl[char_lbls_ngl][i][1]}')'''

    thrust_char_gamit, _ = thrust_indices(fm_gamit[char_lbls_gamit], nodal_gamit[char_lbls_gamit])
    thrust_char_ngl, _ = thrust_indices(fm_ngl[char_lbls_ngl], nodal_ngl[char_lbls_ngl])

    err_loc_TS_gamit = np.sqrt(
        (y_gamit[:, 0][char_gamit][thrust_char_gamit] - pred_TS_gamit[:, 0][char_gamit][thrust_char_gamit]) ** 2 + (
                y_gamit[:, 1][char_gamit][thrust_char_gamit] - pred_TS_gamit[:, 1][char_gamit][
            thrust_char_gamit]) ** 2) * (40075 / 360)
    err_loc_IMG_gamit = np.sqrt(
        (y_gamit[:, 0][char_gamit][thrust_char_gamit] - pred_IMG_gamit[:, 0][char_gamit][thrust_char_gamit]) ** 2 + (
                y_gamit[:, 1][char_gamit][thrust_char_gamit] - pred_IMG_gamit[:, 1][char_gamit][
            thrust_char_gamit]) ** 2) * (40075 / 360)
    err_loc_TRA_gamit = np.sqrt(
        (y_gamit[:, 0][char_gamit][thrust_char_gamit] - pred_TRA_gamit[:, 0][char_gamit][thrust_char_gamit]) ** 2 + (
                y_gamit[:, 1][char_gamit][thrust_char_gamit] - pred_TRA_gamit[:, 1][char_gamit][
            thrust_char_gamit]) ** 2) * (40075 / 360)

    err_loc_TS_ngl = np.sqrt(
        (y_ngl[:, 0][char_ngl][thrust_char_ngl] - pred_TS_ngl[:, 0][char_ngl][thrust_char_ngl]) ** 2 + (
                y_ngl[:, 1][char_ngl][thrust_char_ngl] - pred_TS_ngl[:, 1][char_ngl][thrust_char_ngl]) ** 2) * (
                             40075 / 360)
    err_loc_IMG_ngl = np.sqrt(
        (y_ngl[:, 0][char_ngl][thrust_char_ngl] - pred_IMG_ngl[:, 0][char_ngl][thrust_char_ngl]) ** 2 + (
                y_ngl[:, 1][char_ngl][thrust_char_ngl] - pred_IMG_ngl[:, 1][char_ngl][thrust_char_ngl]) ** 2) * (
                              40075 / 360)
    err_loc_TRA_ngl = np.sqrt(
        (y_ngl[:, 0][char_ngl][thrust_char_ngl] - pred_TRA_ngl[:, 0][char_ngl][thrust_char_ngl]) ** 2 + (
                y_ngl[:, 1][char_ngl][thrust_char_ngl] - pred_TRA_ngl[:, 1][char_ngl][thrust_char_ngl]) ** 2) * (
                              40075 / 360)

    err_mw_TS_gamit = np.abs(
        y_gamit[:, 3][char_gamit][thrust_char_gamit] - pred_TS_gamit[:, 3][char_gamit][thrust_char_gamit])
    err_mw_IMG_gamit = np.abs(
        y_gamit[:, 3][char_gamit][thrust_char_gamit] - pred_IMG_gamit[:, 3][char_gamit][thrust_char_gamit])
    err_mw_TRA_gamit = np.abs(
        y_gamit[:, 3][char_gamit][thrust_char_gamit] - pred_TRA_gamit[:, 3][char_gamit][thrust_char_gamit])

    err_mw_TS_ngl = np.abs(y_ngl[:, 3][char_ngl][thrust_char_ngl] - pred_TS_ngl[:, 3][char_ngl][thrust_char_ngl])
    err_mw_IMG_ngl = np.abs(y_ngl[:, 3][char_ngl][thrust_char_ngl] - pred_IMG_ngl[:, 3][char_ngl][thrust_char_ngl])
    err_mw_TRA_ngl = np.abs(y_ngl[:, 3][char_ngl][thrust_char_ngl] - pred_TRA_ngl[:, 3][char_ngl][thrust_char_ngl])

    dataset_type = ['ISTerre/DD', 'NGL/PPP']
    table = PrettyTable(['Model', f'Position error (km) [{dataset_type[0]}]', f'Magnitude error [{dataset_type[0]}]',
                         f'Position error (km) [{dataset_type[1]}]', f'Magnitude error [{dataset_type[1]}]'])
    table.add_row(['TS', f'{err_loc_TS_gamit.mean():.2f} ± {err_loc_TS_gamit.std():.2f}',
                   f'{err_mw_TS_gamit.mean():.2f} ± {err_mw_TS_gamit.std():.2f}',
                   f'{err_loc_TS_ngl.mean():.2f} ± {err_loc_TS_ngl.std():.2f}',
                   f'{err_mw_TS_ngl.mean():.2f} ± {err_mw_TS_ngl.std():.2f}'])
    table.add_row(['IMG', f'{err_loc_IMG_gamit.mean():.2f} ± {err_loc_IMG_gamit.std():.2f}',
                   f'{err_mw_IMG_gamit.mean():.2f} ± {err_mw_IMG_gamit.std():.2f}',
                   f'{err_loc_IMG_ngl.mean():.2f} ± {err_loc_IMG_ngl.std():.2f}',
                   f'{err_mw_IMG_ngl.mean():.2f} ± {err_mw_IMG_ngl.std():.2f}'])
    table.add_row(['TRA', f'{err_loc_TRA_gamit.mean():.2f} ± {err_loc_TRA_gamit.std():.2f}',
                   f'{err_mw_TRA_gamit.mean():.2f} ± {err_mw_TRA_gamit.std():.2f}',
                   f'{err_loc_TRA_ngl.mean():.2f} ± {err_loc_TRA_ngl.std():.2f}',
                   f'{err_mw_TRA_ngl.mean():.2f} ± {err_mw_TRA_ngl.std():.2f}'])

    print(table)

    print('Statistics on all thrust characterizable events only (median ± MAD:')

    dataset_type = ['ISTerre/DD', 'NGL/PPP']
    table = PrettyTable(['Model', f'Position error (km) [{dataset_type[0]}]', f'Magnitude error [{dataset_type[0]}]',
                         f'Position error (km) [{dataset_type[1]}]', f'Magnitude error [{dataset_type[1]}]'])
    table.add_row(['TS', f'{np.median(err_loc_TS_gamit):.2f} \pm {median_abs_deviation(err_loc_TS_gamit):.2f}',
                   f'{np.median(err_mw_TS_gamit):.2f} \pm {median_abs_deviation(err_mw_TS_gamit):.2f}',
                   f'{np.median(err_loc_TS_ngl):.2f} \pm {median_abs_deviation(err_loc_TS_ngl):.2f}',
                   f'{np.median(err_mw_TS_ngl):.2f} \pm {median_abs_deviation(err_mw_TS_ngl):.2f}'])
    table.add_row(['IMG', f'{np.median(err_loc_IMG_gamit):.2f} \pm {median_abs_deviation(err_loc_IMG_gamit):.2f}',
                   f'{np.median(err_mw_IMG_gamit):.2f} \pm {median_abs_deviation(err_mw_IMG_gamit):.2f}',
                   f'{np.median(err_loc_IMG_ngl):.2f} \pm {median_abs_deviation(err_loc_IMG_ngl):.2f}',
                   f'{np.median(err_mw_IMG_ngl):.2f} \pm {median_abs_deviation(err_mw_IMG_ngl):.2f}'])
    table.add_row(['TRA', f'{np.median(err_loc_TRA_gamit):.2f} \pm {median_abs_deviation(err_loc_TRA_gamit):.2f}',
                   f'{np.median(err_mw_TRA_gamit):.2f} \pm {median_abs_deviation(err_mw_TRA_gamit):.2f}',
                   f'{np.median(err_loc_TRA_ngl):.2f} \pm {median_abs_deviation(err_loc_TRA_ngl):.2f}',
                   f'{np.median(err_mw_TRA_ngl):.2f} \pm {median_abs_deviation(err_mw_TRA_ngl):.2f}'])

    print(table)

    ind_thrust_gamit, ind_non_thrust_gamit = thrust_indices(fm_gamit, nodal_gamit)
    ind_thrust_ngl, ind_non_thrust_ngl = thrust_indices(fm_ngl, nodal_ngl)
    n_thrust_gamit, n_non_thrust_gamit = np.count_nonzero(ind_thrust_gamit), np.count_nonzero(ind_non_thrust_gamit)
    n_thrust_ngl, n_non_thrust_ngl = np.count_nonzero(ind_thrust_ngl), np.count_nonzero(ind_non_thrust_ngl)

    print('Total number of thrust EQ in ISTerre/DD catalog:', n_thrust_gamit)
    print('Total number of non-thrust EQ in ISTerre/DD catalog:', n_non_thrust_gamit)
    print('Total number of thrust and crustal EQ in NGL/PPP catalog:', n_thrust_ngl)
    print('Total number of non-thrust EQ in NGL/PPP catalog:', n_non_thrust_ngl)


def statistics_synthetic_data():
    with np.load('./pred/pred_TS_determ.npz') as f:
        pred_TS, stds_TS, depth_test_TS, y_test_TS, err_mw_TS, err_loc_TS = f['pred'], f['stds'], f['depth_test'], f[
            'y_test'], f['err_mw'], f['err_loc']

    with np.load('./pred/pred_IMG_determ.npz') as f:
        pred_IMG, stds_IMG, depth_test_IMG, y_test_IMG, err_mw_IMG, err_loc_IMG = f['pred'], f['stds'], f['depth_test'], \
                                                                                  f['y_test'], f['err_mw'], f['err_loc']

    with np.load('./pred/pred_TRANSF_determ.npz') as f:
        pred_TRA, stds_TRA, depth_test_TRA, y_test_TRA, err_mw_TRA, err_loc_TRA = f['pred'], f['stds'], f['depth_test'], \
                                                                                  f['y_test'], f['err_mw'], f['err_loc']

    pos_err_TS = np.sqrt((y_test_TS[:, 0] - pred_TS[:, 0]) ** 2 + (y_test_TS[:, 1] - pred_TS[:, 1]) ** 2) * (
            40075 / 360)
    pos_err_IMG = np.sqrt((y_test_IMG[:, 0] - pred_IMG[:, 0]) ** 2 + (y_test_IMG[:, 1] - pred_IMG[:, 1]) ** 2) * (
            40075 / 360)
    pos_err_TRA = np.sqrt((y_test_TRA[:, 0] - pred_TRA[:, 0]) ** 2 + (y_test_TRA[:, 1] - pred_TRA[:, 1]) ** 2) * (
            40075 / 360)

    mw_err_TS = np.abs(y_test_TS[:, 3] - pred_TS[:, 3])
    mw_err_IMG = np.abs(y_test_IMG[:, 3] - pred_IMG[:, 3])
    mw_err_TRA = np.abs(y_test_TRA[:, 3] - pred_TRA[:, 3])

    print('Statistics on all synthetic data (mean ± std:')

    table = PrettyTable(['Model', f'Position error (km)', f'Magnitude error'])
    table.add_row(['TS', f'{np.mean(pos_err_TS):.2f} \pm {np.std(pos_err_TS):.2f}',
                   f'{np.mean(mw_err_TS):.2f} \pm {np.std(mw_err_TS):.2f}'])
    table.add_row(['IMG', f'{np.median(pos_err_IMG):.2f} \pm {np.std(pos_err_IMG):.2f}',
                   f'{np.mean(mw_err_IMG):.2f} \pm {np.std(mw_err_IMG):.2f}'])
    table.add_row(['TRA', f'{np.median(pos_err_TRA):.2f} \pm {np.std(pos_err_TRA):.2f}',
                   f'{np.mean(mw_err_TRA):.2f} \pm {np.std(mw_err_TRA):.2f}'])

    print(table)

    print('Statistics on all synthetic data (median ± MAD:')

    table = PrettyTable(['Model', f'Position error (km)', f'Magnitude error'])
    table.add_row(['TS', f'{np.median(pos_err_TS):.2f} \pm {median_abs_deviation(pos_err_TS):.2f}',
                   f'{np.median(mw_err_TS):.2f} \pm {median_abs_deviation(mw_err_TS):.2f}'])
    table.add_row(['IMG', f'{np.median(pos_err_IMG):.2f} \pm {median_abs_deviation(pos_err_IMG):.2f}',
                   f'{np.median(mw_err_IMG):.2f} \pm {median_abs_deviation(mw_err_IMG):.2f}'])
    table.add_row(['TRA', f'{np.median(pos_err_TRA):.2f} \pm {median_abs_deviation(pos_err_TRA):.2f}',
                   f'{np.median(mw_err_TRA):.2f} \pm {median_abs_deviation(mw_err_TRA):.2f}'])

    print(table)


if __name__ == '__main__':
    _prepare_folders()
    figure1()
    statistics_synthetic_data()
    figure5()
    figure6()
    figure7()
    figure8()
    statistics_real_data()
    figure9()
    figure10()
    figure11()
    figure12()
    figure2()  # at the end in order not to modify font choices for all the figures
