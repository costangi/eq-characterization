import datetime
import os
import sys

import tensorflow as tf

from data_lib.data_utils import prepare_synthetic_data
from ml_lib.IMG_model import IMG
from ml_lib.TRA_model import TRA
from ml_lib.TS_model import TS

if __name__ == '__main__':
    admissible_model_names = ['TS', 'IMG', 'TRA']
    models = [TS, IMG, TRA]

    if len(sys.argv) <= 1:
        raise Exception('Please provide CLI arguments.')
    model_name, data_filename = sys.argv[1], sys.argv[2]
    if model_name not in admissible_model_names:
        raise Exception('Please provide a valid model name.')

    train_data, y_train_scaled, val_data, y_val_scaled, _, _, _ = prepare_synthetic_data(data_filename, model_name)

    model = models[admissible_model_names.index(model_name)]()

    if model_name == 'TS':
        _, n_stations, WIN_LENGTH, _ = train_data[0].shape
        params = {
            "N_t": WIN_LENGTH,
            "N_sub": n_stations,
            "dropout_rate": 0.15,
            "activation": "relu",
            "n_outputs": y_train_scaled.shape[1]
        }

        model.set_params(params)

    model = model.build()
    model.summary()

    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
        loss='mean_squared_error'
    )

    log_dir = f"logs/fit/{model_name}_" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    checkpoint_callback_best = tf.keras.callbacks.ModelCheckpoint(f'best_NEW_{model_name}.h5', save_weights_only=True,
                                                                  save_best_only=True)
    checkpoint_callback_last = tf.keras.callbacks.ModelCheckpoint(f'last_NEW_{model_name}.h5', save_weights_only=True,
                                                                  save_best_only=False)
    checkpoint_path = os.path.join('models', 'TS')
    checkpoint_callback_last_epoch_model = tf.keras.callbacks.ModelCheckpoint(
        filepath=os.path.join(checkpoint_path, 'TS.{epoch:02d}.{val_loss:.4f}.hdf5'), monitor='val_loss',
        save_best_only=True,
        save_weights_only=False)

    callbacks = [checkpoint_callback_best, checkpoint_callback_last, tensorboard_callback,
                 checkpoint_callback_last_epoch_model]

    try:
        model.fit(train_data, y_train_scaled,
                  validation_data=(val_data, y_val_scaled),
                  callbacks=callbacks,
                  verbose=2, epochs=500,
                  batch_size=128)

    except KeyboardInterrupt:
        print(f"Training of {model_name} interrupted and completed")
