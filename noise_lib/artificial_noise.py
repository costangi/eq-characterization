import multiprocessing

import numpy as np
from joblib import Parallel, delayed
from scipy.interpolate import interp1d
from scipy.linalg import eigh

from utils import honshu_coordinates, honshu_filtered_stations


def _valid_original_noise_windows(time, noise_stack_partial, noise_stack, WIN_SIZE=100):
    """
    Computes valid noise windows from the residual time series stack provided in input. A noise window is
    considered valid when there are less than 10 days when no data is present. The remaining data gaps are interpolated.

    Parameters
    ----------
    time : time array
    noise_stack_partial : noise data where noisy stations have been removed.
    noise_stack : full noise data.
    WIN_SIZE : size of the considered windows (default: 100)
    Returns
    -------
    noise_stack_samples : the found noise windows
    """
    n_stations, n_obs, n_dir = noise_stack_partial.shape
    noise_stack_samples = []  # (300, 100, 3)-shaped samples correctly found

    for i in range(WIN_SIZE // 2, n_obs - WIN_SIZE // 2):
        ind_slice = slice(i - WIN_SIZE // 2, i + WIN_SIZE // 2)
        time_slice = time[ind_slice]
        stack_slice = noise_stack_partial[:, ind_slice, :]
        nan_list = np.sum(np.isnan(stack_slice)[:, :, 0], axis=1)  # number of nans in the win for each station

        if np.max(nan_list) > 10:  # -> change window
            continue
        # stack interpolation
        sample = noise_stack[:, ind_slice, :]
        try:
            for j in range(n_stations):
                for k in range(n_dir):
                    nonan = np.argwhere(~np.isnan(stack_slice[j, :, k])).ravel()
                    comp_nonan = stack_slice[j, :, k][nonan].ravel()
                    time_nonan = time_slice[nonan].ravel()
                    interp = interp1d(time_nonan, comp_nonan)  # linear
                    interp_comp = interp(time_slice)
                    interp_comp = interp_comp[
                        np.argwhere(~np.isnan(interp_comp))].ravel()  # pchip sometimes returns NaN values...
                    sample[j, :, k] = interp_comp
        except:
            continue
        noise_stack_samples.append(sample)
    noise_stack_samples = np.array(noise_stack_samples)
    return noise_stack_samples


def _randomize_sample(noise_sample, rnd_phase):
    """
    Generates an artificial noise sample from the noise window provided as input, by performing a Principal
    Component Analysis (PCA), a Fast Fourier Transform (FFT), a phase randomization, an inverse FFT (IFFT) and
    an inverse PCA (reconstruction from eigenvectors).

    Parameters
    ----------
    noise_sample : noise data
    rnd_phase : random phase to be used

    Returns
    -------
    random_sample : artificial noise sample
    """
    n_stations, window_length, n_dir = noise_sample.shape
    random_sample = np.zeros((n_stations, window_length, n_dir))
    random_phase = rnd_phase
    for j in range(n_dir):
        sample = noise_sample[:, :, j]
        mean = np.mean(sample, axis=1).reshape(n_stations, 1)
        sample = sample - mean
        cov = np.cov(sample, rowvar=True)
        w, v = eigh(cov)
        v = np.real(v)
        rotated_noise = np.dot(v.T, sample)
        rotated_noise_ft = np.fft.rfft(rotated_noise, axis=1)  # FT along rows (observations)
        original_phase = np.angle(rotated_noise_ft)
        random_phase[:, 0] = original_phase[:, 0]
        random_phase[:, 50] = original_phase[:, 50]
        ft_s = np.multiply(np.abs(rotated_noise_ft), np.exp(1j * random_phase))
        rotated_noise_rnd = np.real_if_close(np.fft.irfft(ft_s, axis=1))
        sample_rnd = np.dot(v, rotated_noise_rnd)
        sample_rnd = sample_rnd + mean
        random_sample[:, :, j] = sample_rnd
    return random_sample


def generate_artificial_noise(n_samples, residual_folder='./RESIDUAL_DATA'):
    """
    Generates 'n_samples' samples of artificial noise (cf. '_randomize_sample(...)') from real GNSS residuals,
    computed by applying the quadratic trajectory model from Marill et al., 2021.

    Parameters
    ----------
    n_samples : number of samples
    residual_folder : path in which residual GNSS time series are located

    Returns
    -------
    randomized_stack_samples : the 'n_samples' artificial noise time series
    """
    n_dir = 3
    time = np.loadtxt('./RESIDUAL_DATA/3033.txt')[:, 0]
    n_obs = time.shape[0]
    _, _, mask = honshu_filtered_stations()
    station_codes, station_coordinates = honshu_coordinates()
    n_stations = station_coordinates.shape[0]
    noise_stack = np.zeros((n_stations, n_obs, n_dir))
    for i, station in enumerate(station_codes):
        ts = np.loadtxt(f'{residual_folder}/%s.txt' % station)
        noise_stack[i, :, :] = ts[:, (1, 2, 3)]

    noise_stack_samples = _valid_original_noise_windows(time, noise_stack[mask], noise_stack)
    noise_stack_samples = np.nan_to_num(noise_stack_samples, nan=0.)

    random_phases = 2 * np.pi * np.random.random((n_samples, n_stations, 51))
    randomized_stack_samples = np.zeros((n_samples, n_stations, 100, n_dir))

    indices = np.random.randint(0, noise_stack_samples.shape[0], size=n_samples)
    num_cores = multiprocessing.cpu_count()
    results = Parallel(n_jobs=num_cores, verbose=True)(
        delayed(_randomize_sample)(noise_stack_samples[indices[i]], random_phases[i]) for i in range(n_samples))

    for i in range(n_samples):
        randomized_stack_samples[i] = results[i]

    return randomized_stack_samples
