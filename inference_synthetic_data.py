import sys

import numpy as np
import tensorflow as tf

from data_lib.data_utils import prepare_synthetic_data
from ml_lib.IMG_model import IMG
from ml_lib.TRA_model import TRA
from ml_lib.TS_model import TS

if __name__ == '__main__':
    admissible_model_names = ['TS', 'IMG', 'TRA']
    models = [TS, IMG, TRA]

    if len(sys.argv) <= 1:
        raise Exception('Please provide CLI arguments.')
    model_name, data_filename, weight_filename = sys.argv[1], sys.argv[2], sys.argv[3]
    if model_name not in admissible_model_names:
        raise Exception('Please provide a valid model name.')

    _, _, _, _, test_data, y_test, data_scaler = prepare_synthetic_data(data_filename, model_name)

    model = models[admissible_model_names.index(model_name)]()

    if model_name == 'TS':
        _, n_stations, WIN_LENGTH, _ = test_data[0].shape
        params = {
            "N_t": WIN_LENGTH,
            "N_sub": n_stations,
            "dropout_rate": 0.15,
            "activation": "relu",
            "n_outputs": y_test.shape[1]
        }

        model.set_params(params)

    model = model.build()
    model.summary()

    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
        loss='mean_squared_error'
    )

    model.load_weights(weight_filename)

    tmp_res = model.predict(test_data)
    y_pred = data_scaler.inverse_transform(tmp_res)
    np.savez(f'pred_{model_name}', pred=y_pred, y_test=y_test)
